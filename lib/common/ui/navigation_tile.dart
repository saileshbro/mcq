import 'package:flutter/material.dart';
import 'package:mcq/helpers/home_page_tile_icon.dart';

class NavigationTile extends StatelessWidget {
  final Function onPressed;
  final IconData icon;
  final Color color;
  final Color gradientColor;
  final String title;
  Widget trailing;

  NavigationTile({
    Key key,
    @required this.onPressed,
    @required this.icon,
    @required this.color,
    @required this.gradientColor,
    @required this.title,
    this.trailing,
  }) {
    if (this.trailing == null) {
      this.trailing = Icon(
        Icons.arrow_forward_ios,
        size: 18,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onPressed,
      contentPadding: const EdgeInsets.symmetric(horizontal: 24.0),
      leading: HomePageTileIcon(
        color: color,
        icon: icon,
        gradientColor: gradientColor,
        radius: 20,
        iconSize: 20,
      ),
      title: Text(
        title,
        style: Theme.of(context).textTheme.subtitle,
      ),
      trailing: trailing,
    );
  }
}
