import 'package:flutter/material.dart';
import 'package:mcq/constants/application_constants.dart';

class NotificationDot extends StatelessWidget {
  const NotificationDot({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 10,
      width: 10,
      decoration: BoxDecoration(
        color: ApplicationConstants.textRed,
        shape: BoxShape.circle,
        border: Border.all(
          color: Colors.white,
          width: 1.0,
        ),
      ),
    );
  }
}
