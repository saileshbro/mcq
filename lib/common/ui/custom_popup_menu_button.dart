import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/core/services/navigation_service.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/features/login/views/login_view.dart';
import 'package:mcq/features/settings/settings_view.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/locator.dart';

import 'home_tile_model.dart';

class CustomPopupMenuButton extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();
  final UserDataService _userDataService = UserDataService();

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<PopUpItemType>(
      offset: Offset(0, 70),
      icon: Icon(
        Icons.more_vert,
        color: ApplicationConstants.primaryColor,
      ),
      padding: EdgeInsets.fromLTRB(16, 21, 6, 21),
      onSelected: (PopUpItemType item) {
        switch (item) {
          case PopUpItemType.Setting:
            _navigationService.navigateTo(
              SettingsView(
                homeTileModel: HomeTileModel(
                  color: ApplicationConstants.calenderColor,
                  gradientColor: ApplicationConstants.calenderGradientColor,
                  icon: Feather.settings,
                  tileName: Strings.settings,
                ),
              ),
            );
            break;
          case PopUpItemType.Logout:
            _userDataService.clearData();
            _navigationService.navigateTo(LoginView(), clearStack: true);
            break;
          default:
            print(item);
        }
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<PopUpItemType>>[
        PopupMenuItem<PopUpItemType>(
          value: PopUpItemType.Notice,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Feather.bell,
                color: ApplicationConstants.primaryColor,
                size: 18,
              ),
              UiHelper.horizontalSpace(12),
              Text('Notice'),
              UiHelper.horizontalSpace(12),
              CircleAvatar(
                maxRadius: 10,
                backgroundColor: ApplicationConstants.primaryColor,
                child: Text(
                  "4",
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                        color: ApplicationConstants.textFieldBackgroundColor,
                      ),
                ),
              )
            ],
          ),
        ),
        PopupMenuItem<PopUpItemType>(
          value: PopUpItemType.About,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Feather.info,
                color: ApplicationConstants.primaryColor,
                size: 18,
              ),
              UiHelper.horizontalSpace(12),
              Text('About'),
            ],
          ),
        ),
        PopupMenuItem<PopUpItemType>(
          value: PopUpItemType.Setting,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Feather.settings,
                color: ApplicationConstants.primaryColor,
                size: 18,
              ),
              UiHelper.horizontalSpace(12),
              Text('Settings'),
            ],
          ),
        ),
        PopupMenuItem<PopUpItemType>(
          value: PopUpItemType.PrivacyPolicy,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Feather.alert_triangle,
                color: ApplicationConstants.primaryColor,
                size: 18,
              ),
              UiHelper.horizontalSpace(12),
              Text('Privacy Policy'),
            ],
          ),
        ),
        PopupMenuItem<PopUpItemType>(
          value: PopUpItemType.TermOfService,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Feather.alert_triangle,
                color: ApplicationConstants.primaryColor,
                size: 18,
              ),
              UiHelper.horizontalSpace(12),
              Text('Terms of service'),
            ],
          ),
        ),
        PopupMenuItem<PopUpItemType>(
          value: PopUpItemType.Logout,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Feather.log_out,
                color: ApplicationConstants.primaryColor,
                size: 18,
              ),
              UiHelper.horizontalSpace(12),
              Text('Logout'),
            ],
          ),
        ),
      ],
//        onTap: () {
//          Navigator.of(context).push(
//            FadePageTransition(
//              child: SettingsView(
//                homeTileModel: HomeTileModel(
//                  color: ApplicationConstants.calenderColor,
//                  gradientColor: ApplicationConstants.calenderGradientColor,
//                  icon: Feather.settings,
//                  tileName: Strings.settings,
//                ),
//              ),
//            ),
//          );

//        },
//        child: Center(
////          child: Stack(
////            alignment: Alignment.topRight,
////            children: <Widget>[
////              Icon(
////                Icons.notifications,
////                color: ApplicationConstants.primaryColor,
////              ),
////              NotificationDot()
////            ],
////          ),
//          child: Icon(
//            Icons.more_vert,
//            color: ApplicationConstants.primaryColor,
//          ),
//        ),
    );
  }
}

enum PopUpItemType {
  Notice,
  About,
  Setting,
  PrivacyPolicy,
  TermOfService,
  Logout
}
