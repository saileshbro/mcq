import 'package:flutter/widgets.dart';

class CourseTileModel {
  final String title;
  final String imgUrl;

  CourseTileModel({@required this.title, @required this.imgUrl});
}
