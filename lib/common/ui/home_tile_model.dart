import 'package:flutter/widgets.dart';

class HomeTileModel {
  final String tileName;
  final Color color;
  final Color gradientColor;
  final IconData icon;
  String iconHeroTag;
  String textHeroTag;

  HomeTileModel({
    @required this.gradientColor,
    @required this.tileName,
    @required this.color,
    @required this.icon,
  }) {
    iconHeroTag = tileName + "icon";
    textHeroTag = tileName + "text";
  }
}
