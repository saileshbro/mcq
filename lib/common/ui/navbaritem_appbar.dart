import 'package:flutter/material.dart';
import 'package:mcq/common/ui/custom_popup_menu_button.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/helpers/ui_helper.dart';

import 'notification_widget.dart';

class NavBarItemAppBar extends StatelessWidget {
  final String navBarItemTitle;
  final String blackString;
  final String greenString;

  const NavBarItemAppBar({
    Key key,
    @required this.navBarItemTitle,
    @required this.blackString,
    @required this.greenString,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final UserDataService _userDataService = UserDataService();

    return Padding(
      padding:
          EdgeInsets.only(left: 24, top: MediaQuery.of(context).padding.top),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              UiHelper.verticalSpace(24),
              Text(
                this.navBarItemTitle,
                style: Theme.of(context).textTheme.display4,
              ),
              UiHelper.verticalSpace(11),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: this.blackString,
                      style: Theme.of(context).textTheme.caption,
                    ),
                    TextSpan(
                      text: this.greenString,
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(color: ApplicationConstants.primaryColor),
                    ),
                  ],
                ),
              )
            ],
          ),
          CurvedAppBarButton(
            child: CustomPopupMenuButton(),
          ),
        ],
      ),
    );
  }
}
