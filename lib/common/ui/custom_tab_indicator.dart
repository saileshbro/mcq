import 'package:flutter/material.dart';

class CustomTabIndicator extends Decoration {
  final BoxPainter _painter;

  CustomTabIndicator({@required Color color, @required double width})
      : _painter = _CirclePainter(color, width);

  @override
  BoxPainter createBoxPainter([onChanged]) => _painter;
}

class _CirclePainter extends BoxPainter {
  final Paint _paint;
  final double width;

  _CirclePainter(Color color, this.width)
      : _paint = Paint()
          ..color = color
          ..strokeCap = StrokeCap.round
          ..strokeWidth = 4
          ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration cfg) {
    final Offset circleOffset =
        offset + Offset(cfg.size.width / 2 - width / 2, cfg.size.height - 2);
    canvas.drawLine(
        circleOffset, Offset(circleOffset.dx + width, circleOffset.dy), _paint);
  }
}
