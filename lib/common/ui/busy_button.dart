import 'package:flutter/material.dart';
import 'package:mcq/helpers/ui_helper.dart';

import '../../constants/application_constants.dart';

/// A button that shows a busy indicator in place of title
class BusyButton extends StatefulWidget {
  final bool busy;
  final String title;
  final Function onPressed;
  final bool enabled;

  const BusyButton(
      {@required this.title,
      this.busy = false,
      @required this.onPressed,
      this.enabled = true});

  @override
  _BusyButtonState createState() => _BusyButtonState();
}

class _BusyButtonState extends State<BusyButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child: InkWell(
        child: Container(
          height: 48,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: ApplicationConstants.primaryColor,
              borderRadius: BorderRadius.circular(8),
              boxShadow: UiHelper.getBoxShadow(
                  context, ApplicationConstants.primaryColor)),
          child: !widget.busy
              ? Text(
                  widget.title,
                  style: Theme.of(context).textTheme.button.copyWith(
                        color: ApplicationConstants.scaffoldBackgroundColor,
                      ),
                )
              : Container(
                  height: 36,
                  width: 36,
                  padding: EdgeInsets.all(8),
                  child: CircularProgressIndicator(
                    strokeWidth: 2,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                ),
        ),
      ),
    );
  }
}
