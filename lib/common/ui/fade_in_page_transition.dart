library page_transition;

import 'package:flutter/material.dart';

class FadePageTransition<T> extends PageRouteBuilder<T> {
  final Widget child;
  final Curve curve;
  final Alignment alignment;
  final Duration duration;

  FadePageTransition({
    Key key,
    @required this.child,
    this.curve = Curves.linear,
    this.alignment,
    this.duration = const Duration(milliseconds: 300),
    RouteSettings settings,
  }) : super(
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return child;
          },
          transitionDuration: duration,
          settings: settings,
          transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            return FadeTransition(opacity: animation, child: child);
          },
        );
}
