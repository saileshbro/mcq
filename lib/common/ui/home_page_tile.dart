import 'package:flutter/material.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/helpers/home_page_tile_icon.dart';
import 'package:mcq/helpers/ui_helper.dart';

class HomePageTile extends StatelessWidget {
  final HomeTileModel homeTileModel;
  final Function onPressed;

  const HomePageTile(
      {Key key, @required this.homeTileModel, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      borderRadius: BorderRadius.circular(8),
      child: Container(
        height: 100,
        width: 100,
        padding: EdgeInsets.all(8),
        child: Column(
          children: <Widget>[
            Hero(
              tag: homeTileModel.iconHeroTag,
              child: HomePageTileIcon(
                color: homeTileModel.color,
                gradientColor: homeTileModel.gradientColor,
                icon: homeTileModel.icon,
              ),
            ),
            UiHelper.verticalSpace(10),
            Hero(
              tag: homeTileModel.textHeroTag,
              child: Container(
                child: Text(
                  homeTileModel.tileName,
                  style: Theme.of(context).textTheme.subtitle,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
