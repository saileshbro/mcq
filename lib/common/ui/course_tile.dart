import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/theme/theme_model.dart';

class CustomListTile extends StatelessWidget {
  final String title;
  final String subTitle;
  final String imageUrl;
  final Function onTap;
  final bool borderRequired;
  final bool loaded;

  const CustomListTile({
    Key key,
    @required this.title,
    @required this.subTitle,
    @required this.imageUrl,
    @required this.onTap,
    this.loaded = false,
    this.borderRequired = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border(
          bottom: BorderSide(
            color: this.borderRequired
                ? ThemeModel.isDarkTheme(context)
                    ? ApplicationConstants.primaryMediumTextColor
                    : ApplicationConstants.gapColor
                : Colors.transparent,
          ),
        ),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(8),
        onTap: onTap,
        child: ListTile(
          contentPadding: const EdgeInsets.symmetric(horizontal: 8),
          leading: (!loaded)
              ? UiHelper.shimmerContainer(48, 48, radius: 8)
              : CachedNetworkImage(
                  width: 48,
                  height: 48,
                  fit: BoxFit.cover,
                  imageUrl: imageUrl,
                  placeholder: (context, str) =>
                      UiHelper.shimmerContainer(48, 48, radius: 8),
                ),
          title: (!loaded)
              ? Align(
                  alignment: Alignment.topLeft,
                  child: UiHelper.shimmerContainer(72, 14, radius: 2),
                )
              : Text(
                  title,
                  style: Theme.of(context).textTheme.subtitle,
                ),
          subtitle: (!loaded)
              ? Align(
                  alignment: Alignment.topLeft,
                  child: UiHelper.shimmerContainer(128, 14, radius: 2),
                )
              : Text(
                  subTitle,
                  style: Theme.of(context).textTheme.caption,
                ),
          trailing: (!loaded)
              ? null
              : Icon(
                  Icons.arrow_forward_ios,
                  size: 18,
                ),
        ),
      ),
    );
  }
}
