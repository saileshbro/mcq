import 'package:flutter/material.dart';
import 'package:mcq/common/ui/course_tile_model.dart';
import 'package:mcq/common/ui/notification_widget.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/helpers/ui_helper.dart';

import 'custom_popup_menu_button.dart';

class CourseSectionAppBar extends StatelessWidget {
  final CourseTileModel courseTileModel;

  const CourseSectionAppBar({this.courseTileModel});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(left: 16, top: MediaQuery.of(context).padding.top),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              UiHelper.verticalSpace(8),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: UiHelper.getBoxShadow(
                            context, ApplicationConstants.primaryColor),
                      ),
                      child: CircleAvatar(
                        radius: 28,
                        backgroundImage: NetworkImage(
                          this.courseTileModel.imgUrl,
                        ),
                      ),
                    ),
                    UiHelper.horizontalSpace(14),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          this.courseTileModel.title + " ",
                          style: Theme.of(context).textTheme.display4,
                        ),
                        Text(
                          "Enrolled 23hr ago",
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          CurvedAppBarButton(
            child: CustomPopupMenuButton(),
          )
        ],
      ),
    );
  }
}
