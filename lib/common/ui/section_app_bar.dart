import 'package:flutter/material.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/notification_widget.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/helpers/home_page_tile_icon.dart';
import 'package:mcq/helpers/ui_helper.dart';

class SectionAppBar extends StatelessWidget {
  final HomeTileModel homeTileModel;
  final Widget curvedButtonChild;

  const SectionAppBar({this.homeTileModel, @required this.curvedButtonChild});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(left: 16, top: MediaQuery.of(context).padding.top),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              UiHelper.verticalSpace(8),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Row(
                  children: <Widget>[
                    Hero(
                      tag: this.homeTileModel.iconHeroTag,
                      child: HomePageTileIcon(
                        color: this.homeTileModel.color,
                        gradientColor: this.homeTileModel.gradientColor,
                        icon: this.homeTileModel.icon,
                      ),
                    ),
                    UiHelper.horizontalSpace(14),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Hero(
                          tag: homeTileModel.textHeroTag,
                          child: Text(
                            this.homeTileModel.tileName + " ",
                            style: Theme.of(context).textTheme.display4,
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: Strings.neverMissOut,
                                style: Theme.of(context).textTheme.caption,
                              ),
                              TextSpan(
                                text: Strings.events,
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .copyWith(
                                        color:
                                            ApplicationConstants.primaryColor),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          CurvedAppBarButton(
            child: curvedButtonChild,
          )
        ],
      ),
    );
  }
}

class CustomAppBar extends StatelessWidget {
  final HomeTileModel homeTileModel;

  const CustomAppBar({this.homeTileModel});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(left: 16, top: MediaQuery.of(context).padding.top),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
