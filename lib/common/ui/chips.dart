import 'package:flutter/material.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/theme/theme_model.dart';

class Chips extends StatelessWidget {
  final String title;
  final bool isSelected;
  Chips({this.title, this.isSelected});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: isSelected
              ? ApplicationConstants.primaryColor
              : ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.disabledDarkThemeColor
                  : ApplicationConstants.gapColor,
        ),
        padding: EdgeInsets.symmetric(horizontal: 20),
        margin: EdgeInsets.symmetric(horizontal: 4),
        child: Center(
          child: Text(
            this.title,
            style: Theme.of(context).textTheme.subtitle.copyWith(
                color: isSelected
                    ? Colors.white
                    : ThemeModel.isDarkTheme(context)
                        ? ApplicationConstants.gapColor
                        : ApplicationConstants.primaryLightTextColor),
          ),
        ),
      ),
      onTap: () {
        print('bottom tapped');
      },
    );
  }
}
