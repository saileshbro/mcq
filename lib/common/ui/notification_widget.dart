import 'package:flutter/material.dart';

class CurvedAppBarButton extends StatelessWidget {
  final Widget child;

  const CurvedAppBarButton({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 102,
      height: 106,
      decoration: BoxDecoration(
        color: Theme.of(context).inputDecorationTheme.fillColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.elliptical(93, 56),
        ),
      ),
      child: child,
    );
  }
}
