class LoginResponseModel {
  String message;
  String token;
  String name;

  LoginResponseModel({this.message, this.token, this.name});

  LoginResponseModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    token = json['token'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['token'] = this.token;
    data['name'] = this.name;
    return data;
  }
}