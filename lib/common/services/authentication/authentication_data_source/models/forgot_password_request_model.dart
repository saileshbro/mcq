class ForgotPasswordRequestModel {
  String phone;
  String password;
  String code;

  ForgotPasswordRequestModel({this.phone, this.password, this.code});

  ForgotPasswordRequestModel.fromJson(Map<String, dynamic> json) {
    phone = json['phone'];
    password = json['password'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['password'] = this.password;
    data['code'] = this.code;
    return data;
  }
}
