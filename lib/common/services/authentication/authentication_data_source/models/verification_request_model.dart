class VerificationRequestModel {
  String phone;
  int code;

  VerificationRequestModel({this.phone, this.code});

  VerificationRequestModel.fromJson(Map<String, dynamic> json) {
    phone = json['phone'];
    code = int.tryParse(json['code'], radix: 10);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['code'] = this.code;
    return data;
  }
}
