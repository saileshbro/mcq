import 'package:flutter/foundation.dart';

abstract class AuthenticationLDS {
  Future<bool> login({@required String phoneNumber, @required String password});
}
