import 'package:flutter/foundation.dart';
import 'package:mcq/core/models/failure.dart';

import 'authentication_local_ds.dart';

class AuthenticationLMDS implements AuthenticationLDS {
  @override
  Future<bool> login({
    @required String phoneNumber,
    @required String password,
  }) async {
    if (phoneNumber.length != 10) {
      throw Failure(message: "LOCAL MOCK LOGIN: Invalid Phone Number");
    }

    if (password.length < 6) {
      throw Failure(message: "LOCAL MOCK LOGIN: Password is not strong");
    }
    return true;
  }
}
