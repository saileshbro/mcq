import 'package:flutter/foundation.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/forgot_password_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_response_model.dart';
import 'package:mcq/common/services/authentication/repository/authentication_repository.dart';

import '../../../../response_model.dart';

abstract class AuthenticationRDS implements AuthenticationRepository {
  Future<LoginResponseModel> login(
      {@required LoginRequestModel loginRequestModel});

  Future<RegisterResponseModel> register(
      {@required RegisterRequestModel registerRequestModel});

  Future<VerificationResponseModel> verify(
      {@required VerificationRequestModel verificationRequestModel});

  Future<VerificationResponseModel> resendVerify(
      {@required VerificationRequestModel verificationRequestModel});

  Future<ResponseModel> confirmForgotPassword(
      {@required ForgotPasswordRequestModel forgotPasswordRequestModel});
}
