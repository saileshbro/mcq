import 'package:mcq/common/response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/forgot_password_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_response_model.dart';
import 'package:mcq/core/models/failure.dart';

import 'authentication_remote_ds.dart';

class AuthenticationRMDS implements AuthenticationRDS {
  static final String TAG = "REMOTE MOCK";

  @override
  Future<LoginResponseModel> login(
      {LoginRequestModel loginRequestModel}) async {
    if (loginRequestModel.phone.length != 10) {
      throw Failure(message: "$TAG: LOGIN Invalid Phone Number");
    }

    if (loginRequestModel.password.length < 6) {
      throw Failure(message: "$TAG: LOGIN Password is not strong");
    }
    return LoginResponseModel(
      name: "$TAG: LOGIN name",
      message: "$TAG: LOGIN Successful",
      token: "$TAG: LOGIN token_dsafklaksjdflaksjdfl;skjflskdfjslfdjfasdfsadf",
    );
  }

  @override
  Future<RegisterResponseModel> register(
      {RegisterRequestModel registerRequestModel}) async {
    if (registerRequestModel.phone.length != 10) {
      throw Failure(message: "$TAG: REGISTER Invalid Phone Number");
    }

    if (registerRequestModel.password.length < 6) {
      throw Failure(message: "$TAG: REGISTER Password is not strong");
    }

    if (registerRequestModel.name.length < 0) {
      throw Failure(message: "$TAG: REGISTER Invalid Name");
    }
    return RegisterResponseModel(
      message: "$TAG: REGISTER Register Successful",
    );
  }

  @override
  Future<VerificationResponseModel> verify(
      {VerificationRequestModel verificationRequestModel}) {
    // TODO: implement verify
    return null;
  }

  @override
  Future<VerificationResponseModel> resendVerify(
      {VerificationRequestModel verificationRequestModel}) {
    // TODO: implement resendVerify
    return null;
  }

  @override
  Future<ResponseModel> forgotPassword(
      {ForgotPasswordRequestModel forgotPasswordRequestModel}) {
    // TODO: implement forgotPassword
    return null;
  }

  @override
  Future<ResponseModel> confirmForgotPassword(
      {ForgotPasswordRequestModel forgotPasswordRequestModel}) {
    // TODO: implement confirmForgotPassword
    return null;
  }
}
