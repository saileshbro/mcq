import 'dart:convert';

import 'package:mcq/common/response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/forgot_password_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_response_model.dart';
import 'package:mcq/core/services/http_service.dart';

import '../../../../../core/models/failure.dart';
import 'authentication_remote_ds.dart';

class AuthenticationRDSImpl implements AuthenticationRDS {
  final HttpService httpService;

  AuthenticationRDSImpl(this.httpService);

  @override
  Future<LoginResponseModel> login({LoginRequestModel loginRequestModel}) {
    try {
      return httpService
          .post(
              url: 'login', encodedJson: jsonEncode(loginRequestModel.toJson()))
          .handleError((err) {
        throw Failure(
            code: err.code ?? 400, message: err.message ?? "Unusual Exception");
      }).map((_) {
        return LoginResponseModel.fromJson(_);
      }).first;
    } catch (e) {
      throw Failure(
        message: e.toString(),
      );
    }
  }

  @override
  Future<RegisterResponseModel> register(
      {RegisterRequestModel registerRequestModel}) {
    try {
      return httpService
          .post(
              url: 'register_me',
              encodedJson: jsonEncode(registerRequestModel.toJson()))
          .handleError((err) {
        throw Failure(code: err.code, message: err.message);
      }).map((_) {
        return RegisterResponseModel.fromJson(_);
      }).first;
    } catch (e) {
      throw Failure(
        message: e.toString(),
      );
    }
  }

  @override
  Future<VerificationResponseModel> verify(
      {VerificationRequestModel verificationRequestModel}) {
    try {
      return httpService
          .post(
              url: "verify_user",
              encodedJson: jsonEncode(verificationRequestModel.toJson()))
          .handleError((err) {
            throw Failure(message: err.toString());
          })
          .map((_) => VerificationResponseModel.fromJson(_))
          .first;
    } catch (e) {
      throw Failure(
        message: e.toString(),
      );
    }
  }

  @override
  Future<VerificationResponseModel> resendVerify(
      {VerificationRequestModel verificationRequestModel}) {
    try {
      return httpService
          .post(
              url: "resend_verify_code",
              encodedJson: jsonEncode(verificationRequestModel.toJson()))
          .handleError((err) {
            throw Failure(message: err.toString());
          })
          .map((_) => VerificationResponseModel.fromJson(_))
          .first;
    } catch (e) {
      throw Failure(
        message: e.toString(),
      );
    }
  }

  @override
  Future<ResponseModel> forgotPassword(
      {ForgotPasswordRequestModel forgotPasswordRequestModel}) {
    try {
      return httpService
          .post(
              url: "forgot_password",
              encodedJson: jsonEncode(forgotPasswordRequestModel.toJson()))
          .handleError((err) {
            throw Failure(message: err.toString());
          })
          .map((_) => ResponseModel.fromJson(_))
          .first;
    } catch (e) {
      throw Failure(
        message: e.toString(),
      );
    }
  }

  @override
  Future<ResponseModel> confirmForgotPassword(
      {ForgotPasswordRequestModel forgotPasswordRequestModel}) {
    try {
      return httpService
          .post(
              url: "confirm_forgot_password",
              encodedJson: jsonEncode(forgotPasswordRequestModel.toJson()))
          .handleError((err) {
            throw Failure(message: err.toString());
          })
          .map((_) => ResponseModel.fromJson(_))
          .first;
    } catch (e) {
      throw Failure(
        message: e.toString(),
      );
    }
  }
}
