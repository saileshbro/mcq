import 'package:flutter/foundation.dart';
import 'package:mcq/common/response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/local/authentication_local_ds.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/forgot_password_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/remote/authentication_remote_ds.dart';
import 'package:mcq/common/services/authentication/repository/authentication_repository.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationLDS localDataSource;
  final AuthenticationRDS remoteDataSource;

  AuthenticationRepositoryImpl({
    @required this.localDataSource,
    @required this.remoteDataSource,
  });

  @override
  Future<LoginResponseModel> login(
      {@required LoginRequestModel loginRequestModel}) {
    // Todo: as required use local and remote
    return remoteDataSource.login(loginRequestModel: loginRequestModel);
  }

  @override
  Future<RegisterResponseModel> register(
      {RegisterRequestModel registerRequestModel}) {
    // Todo: as required use local and remote
    return remoteDataSource.register(
        registerRequestModel: registerRequestModel);
  }

  @override
  Future<VerificationResponseModel> verify(
      {VerificationRequestModel verificationRequestModel}) {
    // Todo: as required use local and remote
    return remoteDataSource.verify(
        verificationRequestModel: verificationRequestModel);
  }

  @override
  Future<VerificationResponseModel> resendVerify(
      {VerificationRequestModel verificationRequestModel}) {
    // Todo: as required use local and remote
    return remoteDataSource.resendVerify(
      verificationRequestModel: verificationRequestModel,
    );
  }

  @override
  Future<ResponseModel> forgotPassword(
      {ForgotPasswordRequestModel forgotPasswordRequestModel}) {
    // TODO: implement forgotPassword
    return remoteDataSource.forgotPassword(
        forgotPasswordRequestModel: forgotPasswordRequestModel);
  }

  @override
  Future<ResponseModel> confirmForgotPassword(
      {ForgotPasswordRequestModel forgotPasswordRequestModel}) {
    // TODO: implement confirmForgotPassword
    return remoteDataSource.confirmForgotPassword(
        forgotPasswordRequestModel: forgotPasswordRequestModel);
  }
}
