import 'package:flutter/cupertino.dart';
import 'package:mcq/common/services/home/home_data_source/local/home_local_ds.dart';
import 'package:mcq/common/services/home/home_data_source/models/course_respoonse_model.dart';
import 'package:mcq/common/services/home/home_data_source/models/recommended_courses_response_model.dart';
import 'package:mcq/common/services/home/home_data_source/remote/home_remote_ds.dart';
import 'package:mcq/common/services/home/repository/home_repository.dart';

class HomeRepositoryImpl implements HomeRepository {
  final HomeLDS localDataSource;
  final HomeRDS remoteDataSource;

  HomeRepositoryImpl(
      {@required this.localDataSource, @required this.remoteDataSource});

  @override
  Future<CourseResponseModel> getCourses() {
    return remoteDataSource.getCourses();
  }

  @override
  Future<RecommendedCoursesResponseModel> getRecommendedCourses() {
    // TODO: implement getRecommendedCourses
    return remoteDataSource.getRecommendedCourses();
  }
}
