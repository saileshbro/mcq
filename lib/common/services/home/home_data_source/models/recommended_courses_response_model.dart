import 'package:mcq/common/services/home/home_data_source/models/course_respoonse_model.dart';

class RecommendedCoursesResponseModel {
  String lastViewed;
  List<Course> recommendedCourses;

  RecommendedCoursesResponseModel({this.lastViewed, this.recommendedCourses});

  RecommendedCoursesResponseModel.fromJson(Map<String, dynamic> json) {
    lastViewed = json['last_viewed'];
    if (json['recommended_courses'] != null) {
      recommendedCourses = new List<Course>();
      json['recommended_courses'].forEach((v) {
        recommendedCourses.add(new Course.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['last_viewed'] = this.lastViewed;
    if (this.recommendedCourses != null) {
      data['recommended_courses'] =
          this.recommendedCourses.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
