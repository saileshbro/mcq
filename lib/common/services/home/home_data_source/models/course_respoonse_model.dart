class CourseResponseModel {
  List<Course> course;

  CourseResponseModel({this.course});

  CourseResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['course'] != null) {
      course = new List<Course>();
      json['course'].forEach((v) {
        course.add(new Course.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.course != null) {
      data['course'] = this.course.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Course {
  int id;
  String name;
  String picture;
  String createdAt;
  String contentDescription;
  String courseDescription;
  String price;

  Course(
      {this.id,
      this.name,
      this.picture,
      this.createdAt,
      this.contentDescription,
      this.courseDescription,
      this.price});

  Course.fromJson(Map<String, dynamic> json) {
    id = int.tryParse(json['id'], radix: 10);
    name = json['name'];
    picture = json['picture'];
    createdAt = json['created_at'];
    contentDescription = json['content_description'];
    courseDescription = json['course_description'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['picture'] = this.picture;
    data['created_at'] = this.createdAt;
    data['content_description'] = this.contentDescription;
    data['course_description'] = this.courseDescription;
    data['price'] = this.picture;
    return data;
  }
}
