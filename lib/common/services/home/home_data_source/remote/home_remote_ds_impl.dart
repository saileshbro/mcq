import 'package:mcq/common/services/home/home_data_source/models/course_respoonse_model.dart';
import 'package:mcq/common/services/home/home_data_source/models/recommended_courses_response_model.dart';
import 'package:mcq/common/services/home/home_data_source/remote/home_remote_ds.dart';
import 'package:mcq/core/models/failure.dart';
import 'package:mcq/core/services/http_service.dart';

class HomeRDSImpl implements HomeRDS {
  final HttpService httpService;

  HomeRDSImpl(this.httpService);

  @override
  Future<CourseResponseModel> getCourses() {
    try {
      return httpService.get(url: "course").handleError((err) {
        throw Failure(
            code: err.code ?? 400, message: err.message ?? "Unusual Exception");
      }).map((_) {
        return CourseResponseModel.fromJson(_);
      }).first;
    } catch (e) {
      throw Failure(
        message: e.toString(),
      );
    }
  }

  @override
  Future<RecommendedCoursesResponseModel> getRecommendedCourses() {
    try {
      return httpService.get(url: "recommended_course").handleError((err) {
        throw Failure(
            code: err?.code ?? 400,
            message: err?.message ?? "Unusual Exception");
      }).map((_) {
        return RecommendedCoursesResponseModel.fromJson(_);
      }).first;
    } catch (e) {
      throw Failure(
        message: e.toString(),
      );
    }
  }
}
