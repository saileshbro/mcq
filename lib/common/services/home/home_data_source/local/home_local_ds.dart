import 'package:mcq/common/services/home/home_data_source/models/course_respoonse_model.dart';
import 'package:mcq/common/services/home/home_data_source/models/recommended_courses_response_model.dart';

abstract class HomeLDS {
  Future<RecommendedCoursesResponseModel> getRecommendedCourses();

  Future<CourseResponseModel> getCourses();
}
