import 'package:flutter/material.dart';
import 'package:mcq/common/ui/custom_popup_menu_button.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/section_app_bar.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/theme/theme_model.dart';
import 'package:provider_architecture/provider_architecture.dart';

class SettingsView extends ProviderWidget<ThemeModel> {
  final HomeTileModel homeTileModel;

  SettingsView({Key key, @required this.homeTileModel}) : super(key: key);

  bool _nightMode;

  @override
  Widget build(BuildContext context, ThemeModel model) {
    _nightMode = ThemeModel.isDarkTheme(context);

    return Scaffold(
      body: Column(
        children: <Widget>[
          SectionAppBar(
            homeTileModel: homeTileModel,
            curvedButtonChild: CustomPopupMenuButton(),
          ),
          UiHelper.verticalSpace(36),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(Strings.nightMode,
                        style: Theme.of(context).textTheme.display3),
                    Switch.adaptive(
                      activeColor: ApplicationConstants.primaryColor,
                      activeTrackColor:
                          ApplicationConstants.primaryMediumTextColor,
                      inactiveTrackColor:
                          ApplicationConstants.textFieldBackgroundColor,
                      onChanged: (val) {
                        print("onj");
                        _nightMode = val;
                        model.switchTheme();
                      },
                      value: _nightMode,
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
