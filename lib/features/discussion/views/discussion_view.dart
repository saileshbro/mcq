import 'package:flutter/material.dart';
import 'package:mcq/common/ui/navbaritem_appbar.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/helpers/ui_helper.dart';

class DiscussionView extends StatefulWidget {
  @override
  _DiscussionViewState createState() => _DiscussionViewState();
}

class _DiscussionViewState extends State<DiscussionView> {
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          NavBarItemAppBar(
            navBarItemTitle: Strings.discussions,
            blackString: Strings.chooseYour,
            greenString: Strings.rightAway,
          ),
          UiHelper.verticalSpace(10),
        ],
      ),
    );
  }
}
