import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/features/login/views/login_view.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnboardingView extends StatefulWidget {
  @override
  _OnboardingViewState createState() => _OnboardingViewState();
}

class _OnboardingViewState extends State<OnboardingView> {
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  List<_OnboardingModel> onboardingItems = [
    _OnboardingModel(
      headLine: Strings.learn,
      baseLine: Strings.readOurBlog,
      asset: ApplicationConstants.onboardingAsset1,
    ),
    _OnboardingModel(
      headLine: Strings.practice,
      baseLine: Strings.readOurBlog,
      asset: ApplicationConstants.onboardingAsset2,
    ),
    _OnboardingModel(
      headLine: Strings.score,
      baseLine: Strings.readOurBlog,
      asset: ApplicationConstants.onboardingAsset3,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: <Widget>[
          UiHelper.verticalSpace(28),
          Container(
            height: MediaQuery.of(context).size.height * 0.65,
            child: PageView.builder(
              physics: BouncingScrollPhysics(),
              controller: _pageController,
              itemCount: onboardingItems.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 41),
                  child: _OnboardingFront(
                    onboardingModel: onboardingItems[index],
                  ),
                );
              },
            ),
          ),
          UiHelper.verticalSpace(30),
          SmoothPageIndicator(
            controller: _pageController,
            count: onboardingItems.length,
            effect: WormEffect(
              activeDotColor: ApplicationConstants.primaryColor,
              dotColor: ApplicationConstants.primaryColor,
              paintStyle: PaintingStyle.stroke,
              dotHeight: 10.0,
              dotWidth: 10.0,
              radius: 10.0,
              strokeWidth: 1.5,
            ),
          ),
          Spacer(),
          Container(
            decoration: BoxDecoration(
              boxShadow: UiHelper.getBoxShadow(
                context,
                ApplicationConstants.primaryColor,
              ),
            ),
            width: 112,
            child: FlatButton(
              color: ApplicationConstants.primaryColor,
              child: Icon(Entypo.arrow_long_right),
              onPressed: () {
                SystemChrome.setPreferredOrientations([
                  DeviceOrientation.landscapeRight,
                  DeviceOrientation.landscapeLeft,
                  DeviceOrientation.portraitUp,
                  DeviceOrientation.portraitDown,
                ]);
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => LoginView()))
                    .then((onValue) {
                  SystemChrome.setPreferredOrientations([
                    DeviceOrientation.portraitDown,
                    DeviceOrientation.portraitUp,
                  ]);
                });
              },
            ),
          ),
          UiHelper.verticalSpace(45),
        ],
      ),
    );
  }
}

class _OnboardingFront extends StatelessWidget {
  final _OnboardingModel onboardingModel;

  const _OnboardingFront({Key key, @required this.onboardingModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Image.asset(
            onboardingModel.asset,
            height: MediaQuery.of(context).size.width * 0.65,
          ),
          Column(
            children: <Widget>[
              UiHelper.verticalSpace(58),
              Text(
                onboardingModel.headLine,
                style: Theme.of(context).textTheme.display4,
              ),
              UiHelper.verticalSpace(28),
              Text(
                onboardingModel.baseLine,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          )
        ],
      ),
    );
  }
}

class _OnboardingModel {
  final String headLine;
  final String baseLine;
  final String asset;

  _OnboardingModel({
    @required this.headLine,
    @required this.baseLine,
    @required this.asset,
  });
}
