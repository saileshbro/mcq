import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/common/ui/gap.dart';
import 'package:mcq/common/ui/navbaritem_appbar.dart';
import 'package:mcq/common/ui/navigation_tile.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/theme/theme_model.dart';

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  final UserDataService _userDataService = UserDataService();
  final attemptedQuestion = '4120';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          NavBarItemAppBar(
            navBarItemTitle: Strings.profile,
            blackString: Strings.status,
            greenString: Strings.activelyInvolved,
          ),
          UiHelper.verticalSpace(28),
          ProfileFront(
            imageUrl:
                "https://avatars0.githubusercontent.com/u/41059790?s=460&v=4",
            attemps: this.attemptedQuestion,
            highestRank: '44',
            hightestScore: '76',
            name: _userDataService.name,
            timeSpent: '50 hrs',
          ),
          UiHelper.verticalSpace(24),
          Gap(),
          UiHelper.verticalSpace(24),
          Column(
            children: <Widget>[
              NavigationTile(
                icon: Feather.video,
                onPressed: () {},
                title: Strings.examHistory,
                color: ApplicationConstants.videosColor,
                gradientColor: ApplicationConstants.videosGradientColor,
              ),
              NavigationTile(
                icon: Feather.edit_3,
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      this.attemptedQuestion,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle
                          .copyWith(color: ApplicationConstants.primaryColor),
                    ),
                    UiHelper.horizontalSpace(16),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 18,
                    ),
                  ],
                ),
                onPressed: () {},
                title: Strings.attemptedQuestion,
                color: ApplicationConstants.practiceColor,
                gradientColor: ApplicationConstants.practiceGradientColor,
              ),
              NavigationTile(
                icon: Feather.file_text,
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "4210",
                      style: Theme.of(context)
                          .textTheme
                          .subtitle
                          .copyWith(color: ApplicationConstants.primaryColor),
                    ),
                    UiHelper.horizontalSpace(16),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 18,
                    ),
                  ],
                ),
                onPressed: () {},
                title: Strings.enrolledCourses,
                color: ApplicationConstants.notesColor,
                gradientColor: ApplicationConstants.notesGradientColor,
              ),
              NavigationTile(
                icon: Feather.bookmark,
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "4210",
                      style: Theme.of(context)
                          .textTheme
                          .subtitle
                          .copyWith(color: ApplicationConstants.primaryColor),
                    ),
                    UiHelper.horizontalSpace(16),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 18,
                    ),
                  ],
                ),
                onPressed: () {},
                title: Strings.savedResources,
                color: ApplicationConstants.calenderColor,
                gradientColor: ApplicationConstants.calenderGradientColor,
              ),
            ],
          )
        ],
      ),
    );
  }
}

class ProfileFront extends StatelessWidget {
  final String name;
  final String imageUrl;
  final String highestRank;
  final String attemps;
  final String timeSpent;
  final String hightestScore;

  const ProfileFront({
    Key key,
    @required this.name,
    @required this.imageUrl,
    @required this.highestRank,
    @required this.attemps,
    @required this.timeSpent,
    @required this.hightestScore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: UiHelper.getBoxShadow(
                            context, ApplicationConstants.primaryColor)),
                    child: CircleAvatar(
                      backgroundImage: CachedNetworkImageProvider(
                        this.imageUrl,
                      ),
                      backgroundColor: ApplicationConstants.primaryColor,
                      maxRadius: 32.0,
                    ),
                  ),
                  UiHelper.horizontalSpace(20),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        this.name,
                        style: Theme.of(context).textTheme.button,
                      ),
                      UiHelper.verticalSpace(4),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: Strings.highestRanking,
                              style: Theme.of(context).textTheme.caption,
                            ),
                            TextSpan(
                              text: this.highestRank,
                              style: Theme.of(context)
                                  .textTheme
                                  .caption
                                  .copyWith(
                                      color: ApplicationConstants.primaryColor),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
              IconButton(
                icon: Icon(Feather.edit),
                onPressed: () {
                  UserDataService().clearData();
                },
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
          UiHelper.verticalSpace(32),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    attemps,
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                  UiHelper.verticalSpace(8),
                  Text(
                    "Attemps",
                    style: Theme.of(context).textTheme.subtitle.copyWith(
                          color: ThemeModel.isDarkTheme(context)
                              ? ApplicationConstants.disabledDarkThemeColor
                              : ApplicationConstants.disabledLightThemeColor,
                        ),
                  )
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                    this.timeSpent,
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                  UiHelper.verticalSpace(8),
                  Text(
                    "Time Spent",
                    style: Theme.of(context).textTheme.subtitle.copyWith(
                          color: ThemeModel.isDarkTheme(context)
                              ? ApplicationConstants.disabledDarkThemeColor
                              : ApplicationConstants.disabledLightThemeColor,
                        ),
                  )
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                    this.hightestScore,
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                  UiHelper.verticalSpace(8),
                  Text(
                    "Highest",
                    style: Theme.of(context).textTheme.subtitle.copyWith(
                          color: ThemeModel.isDarkTheme(context)
                              ? ApplicationConstants.disabledDarkThemeColor
                              : ApplicationConstants.disabledLightThemeColor,
                        ),
                  )
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
