import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/section_app_bar.dart';

class NoteView extends StatefulWidget {
  final HomeTileModel homeTileModel;

  const NoteView({Key key, @required this.homeTileModel}) : super(key: key);

  @override
  _NoteViewState createState() => _NoteViewState();
}

class _NoteViewState extends State<NoteView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.zero,
        child: AppBar(),
      ),
      endDrawer: Builder(
        builder: (BuildContext _) {
          return Drawer(
            child: Text("hi"),
          );
        },
      ),
      body: Column(
        children: <Widget>[
          SectionAppBar(
            homeTileModel: widget.homeTileModel,
            curvedButtonChild: Builder(
              builder: (context) => IconButton(
                  onPressed: () {
                    print("drawer");
                    Scaffold.of(context).openEndDrawer();
                  },
                  icon: Icon(Feather.message_circle)),
            ),
          )
        ],
      ),
    );
  }
}
