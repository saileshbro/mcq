import 'package:mcq/common/services/home/home_data_source/models/course_respoonse_model.dart';
import 'package:mcq/common/services/home/home_data_source/models/recommended_courses_response_model.dart';
import 'package:mcq/common/services/home/repository/home_repository.dart';
import 'package:mcq/core/models/base_view_model.dart';
import 'package:mcq/core/models/failure.dart';
import 'package:mcq/core/services/dialog_service.dart';
import 'package:mcq/locator.dart';

class HomeViewModel extends BaseViewModel {
  HomeRepository _homeRepository = locator<HomeRepository>();
  DialogService _dialogService = locator<DialogService>();
  RecommendedCoursesResponseModel recommendedCoursesResponseModel =
      RecommendedCoursesResponseModel();

  Future getRecommendedCourses() async {
    setBusy(true);
    try {
      recommendedCoursesResponseModel =
          await _homeRepository.getRecommendedCourses();
      setBusy(false);

//      return recommendedCoursesResponseModel;
    } on Failure catch (e) {
      _dialogService.showDialog(
        title: "Error fetching recommended courses",
        description: e.message,
        buttonTitle: "Ok",
      );
//      return false;
    }
  }

  Future getCourses() async {
    setBusy(true);

    try {
      CourseResponseModel courseResponseModel =
          await _homeRepository.getCourses();
      setBusy(false);
      return courseResponseModel;
    } on Failure catch (e) {
      _dialogService.showDialog(
        title: "Error Fetching courses",
        description: e.message,
        buttonTitle: "Ok",
      );

      return false;
    }
  }
}
