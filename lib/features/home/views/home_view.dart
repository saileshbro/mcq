import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/common/ui/course_tile.dart';
import 'package:mcq/common/ui/fade_in_page_transition.dart';
import 'package:mcq/common/ui/gap.dart';
import 'package:mcq/common/ui/home_page_tile.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/navbaritem_appbar.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/features/calender/views/calender_view.dart';
import 'package:mcq/features/exams/views/exams_view.dart';
import 'package:mcq/features/home/models/home_view_model.dart';
import 'package:mcq/features/notes/views/all_notes_view.dart';
import 'package:mcq/features/practice/views/practice_view.dart';
import 'package:mcq/features/results/views/results_view.dart';
import 'package:mcq/features/videos/views/videos_view.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:provider_architecture/viewmodel_provider.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final UserDataService _userDataService = UserDataService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ViewModelProvider<HomeViewModel>.withConsumer(
        viewModel: HomeViewModel(),
        onModelReady: (model) {
          model.getRecommendedCourses();
        },
        builder: (context, homeModel, _) {
          return RefreshIndicator(
            onRefresh: () async {
              return Future.delayed(Duration(seconds: 2));
            },
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                NavBarItemAppBar(
                  greenString: (_userDataService.name != null &&
                          _userDataService.name.length > 0)
                      ? _userDataService.name
                      : "Guest",
                  blackString: Strings.welcome + ", ",
                  navBarItemTitle: Strings.home,
                ),
                UiHelper.verticalSpace(20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: TextFormField(
                    style: Theme.of(context).textTheme.caption,
                    autocorrect: false,
                    decoration: InputDecoration(
                      hintStyle: Theme.of(context).textTheme.caption.copyWith(
                          color: ApplicationConstants.disabledLightThemeColor),
                      prefixIcon: Icon(
                        Icons.search,
                      ),
                      hintText: Strings.search,
                    ),
                  ),
                ),
                UiHelper.verticalSpace(36),
                _HomeTilesSection(),
                UiHelper.verticalSpace(24),
                Gap(),
                UiHelper.verticalSpace(16),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        Strings.recommendedCourses,
                        style: Theme.of(context).textTheme.display3,
                      ),
                      UiHelper.verticalSpace(8),
                      homeModel.busy
                          ? UiHelper.shimmerContainer(140, 10)
                          : RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: Strings.lastViewed,
                                    style: Theme.of(context).textTheme.caption,
                                  ),
                                  TextSpan(
                                    text: homeModel
                                        .recommendedCoursesResponseModel
                                        .lastViewed,
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption
                                        .copyWith(
                                            color: ApplicationConstants
                                                .primaryColor),
                                  ),
                                ],
                              ),
                            ),
                    ],
                  ),
                ),
                UiHelper.verticalSpace(16),
                Column(
                  children: homeModel.busy
                      ? [
                          CustomListTile(
                            imageUrl: 'assets/icons/physics.png',
                            title: "Physics",
                            subTitle: "400+ course content",
                            onTap: () {},
                          ),
                          CustomListTile(
                            imageUrl: 'assets/icons/physics.png',
                            title: "Physics",
                            subTitle: "400+ course content",
                            onTap: () {},
                          ),
                          CustomListTile(
                            imageUrl: 'assets/icons/physics.png',
                            title: "Physics",
                            subTitle: "400+ course content",
                            onTap: () {},
                          ),
                          CustomListTile(
                            imageUrl: 'assets/icons/physics.png',
                            title: "Physics",
                            subTitle: "400+ course content",
                            borderRequired: false,
                            onTap: () {},
                          ),
                        ]
                      : [
                          for (int i = 0;
                              i <
                                  homeModel.recommendedCoursesResponseModel
                                      .recommendedCourses.length;
                              i++)
                            CustomListTile(
                              imageUrl: homeModel
                                  .recommendedCoursesResponseModel
                                  .recommendedCourses[i]
                                  .picture,
                              title: homeModel.recommendedCoursesResponseModel
                                  .recommendedCourses[i].name,
                              subTitle: "400+ course content",
                              borderRequired: i !=
                                  homeModel.recommendedCoursesResponseModel
                                          .recommendedCourses.length -
                                      1,
                              loaded: true,
                              onTap: () {},
                            ),
                        ],
                )
              ],
            ),
          );
        },
      ),
    );
  }
}

class _HomeTilesSection extends StatelessWidget {
  const _HomeTilesSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              HomePageTile(
                homeTileModel: homeTiles[0],
                onPressed: () {
                  Navigator.of(context).push(
                    FadePageTransition(
                      child: AllNotesView(
                        homeTileModel: homeTiles[0],
                      ),
                    ),
                  );
                },
              ),
              HomePageTile(
                homeTileModel: homeTiles[1],
                onPressed: () {
                  Navigator.of(context).push(
                    FadePageTransition(
                      child: VideosView(
                        homeTileModel: homeTiles[1],
                      ),
                    ),
                  );
                },
              ),
              HomePageTile(
                homeTileModel: homeTiles[2],
                onPressed: () {
                  Navigator.of(context).push(
                    FadePageTransition(
                      child: PracticeView(
                        homeTileModel: homeTiles[2],
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
        UiHelper.verticalSpace(25),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              HomePageTile(
                homeTileModel: homeTiles[3],
                onPressed: () {
                  Navigator.of(context).push(
                    FadePageTransition(
                      child: ExamsView(
                        homeTileModel: homeTiles[3],
                      ),
                    ),
                  );
                },
              ),
              HomePageTile(
                homeTileModel: homeTiles[4],
                onPressed: () {
                  Navigator.of(context).push(
                    FadePageTransition(
                      child: CalenderView(
                        homeTileModel: homeTiles[4],
                      ),
                    ),
                  );
                },
              ),
              HomePageTile(
                homeTileModel: homeTiles[5],
                onPressed: () {
                  Navigator.of(context).push(
                    FadePageTransition(
                      child: ResultsView(
                        homeTileModel: homeTiles[5],
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}

List<HomeTileModel> homeTiles = [
  HomeTileModel(
    tileName: Strings.notes,
    color: ApplicationConstants.notesColor,
    gradientColor: ApplicationConstants.notesGradientColor,
    icon: Feather.file_text,
  ),
  HomeTileModel(
    tileName: Strings.videos,
    color: ApplicationConstants.videosColor,
    gradientColor: ApplicationConstants.videosGradientColor,
    icon: Feather.video,
  ),
  HomeTileModel(
    tileName: Strings.practice,
    color: ApplicationConstants.practiceColor,
    gradientColor: ApplicationConstants.practiceGradientColor,
    icon: Feather.book_open,
  ),
  HomeTileModel(
    tileName: Strings.exams,
    color: ApplicationConstants.examsColor,
    gradientColor: ApplicationConstants.examsGradientColor,
    icon: Feather.edit_3,
  ),
  HomeTileModel(
    tileName: Strings.calender,
    color: ApplicationConstants.calenderColor,
    gradientColor: ApplicationConstants.calenderGradientColor,
    icon: Feather.calendar,
  ),
  HomeTileModel(
    tileName: Strings.results,
    color: ApplicationConstants.resultsColor,
    gradientColor: ApplicationConstants.resultsGradientColor,
    icon: Feather.award,
  ),
];
