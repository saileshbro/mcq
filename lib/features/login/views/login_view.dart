import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/common/ui/busy_button.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/features/login/models/forgot_password_view_model.dart';
import 'package:mcq/features/login/models/login_view_model.dart';
import 'package:mcq/features/register/views/register_view.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:provider_architecture/viewmodel_provider.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  FocusNode phoneFocusNode;
  FocusNode passwordFocusNode;
  bool obscureText = true;

  TextEditingController _phoneNumberController, _passwordController;

  @override
  void initState() {
    phoneFocusNode = FocusNode();
    passwordFocusNode = FocusNode();

    _phoneNumberController = TextEditingController(text: "12345");
    _passwordController = TextEditingController(text: "user123");

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: true,
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Navigator.canPop(context) ? AppBar() : SizedBox.shrink(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32),
                  child: Image.asset(
                    ApplicationConstants.loginIllustration,
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
                UiHelper.verticalSpace(20),
                Text(
                  Strings.welcome,
                  style: Theme.of(context).textTheme.display4,
                ),
                UiHelper.verticalSpace(9),
                Text(
                  Strings.afterLogin,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                ),
                UiHelper.verticalSpace(26),
                ViewModelProvider<LoginViewModel>.withConsumer(
                    viewModel: LoginViewModel(),
                    builder: (context, model, child) {
                      return Form(
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              controller: _phoneNumberController,
                              keyboardType: TextInputType.number,
                              focusNode: phoneFocusNode,
                              onEditingComplete: () {
                                FocusScope.of(context)
                                    .requestFocus(passwordFocusNode);
                              },
                              style: Theme.of(context).textTheme.caption,
                              decoration: InputDecoration(
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                        color: ApplicationConstants
                                            .disabledLightThemeColor),
                                prefixIcon: Icon(
                                  Icons.phone_android,
                                ),
                                hintText: Strings.phoneNumber,
                              ),
                            ),
                            UiHelper.verticalSpace(16),
                            TextFormField(
                              controller: _passwordController,
                              focusNode: passwordFocusNode,
                              style: Theme.of(context).textTheme.caption,
                              onEditingComplete: () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                              },
                              obscureText: obscureText,
                              decoration: InputDecoration(
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                        color: ApplicationConstants
                                            .disabledLightThemeColor),
                                prefixIcon: Icon(
                                  Icons.lock_open,
                                ),
                                suffixIcon: Material(
                                  type: MaterialType.transparency,
                                  shape: CircleBorder(),
                                  child: InkWell(
                                    customBorder: CircleBorder(),
                                    child: Icon(obscureText
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                    onTap: () {
                                      setState(() {
                                        obscureText = !obscureText;
                                      });
                                    },
                                  ),
                                ),
                                hintText: Strings.password,
                              ),
                            ),
                            UiHelper.verticalSpace(16),
                            Align(
                              alignment: Alignment.centerRight,
                              child: GestureDetector(
                                onTap: () {
                                  UiHelper.showBottomSheet(
                                    context,
                                    child: ForgotPasswordBottomSheet(),
                                  );
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 3.0),
                                  child: Text(
                                    Strings.forgotPassword,
                                    style: Theme.of(context).textTheme.button,
                                  ),
                                ),
                              ),
                            ),
                            UiHelper.verticalSpace(16),
                            Container(
                              decoration: BoxDecoration(
                                boxShadow: UiHelper.getBoxShadow(
                                  context,
                                  ApplicationConstants.primaryColor,
                                ),
                              ),
                              width: double.infinity,
                              child: BusyButton(
//                                color: ApplicationConstants.primaryColor,
                                title: (Strings.login),
                                busy: model.busy,
                                onPressed: model.busy
                                    ? null
                                    : () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        model.login(_phoneNumberController.text,
                                            _passwordController.text);
                                      },
                              ),
                            ),
                            FlatButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: ($) => RegisterView()));
                              },
                              splashColor: Colors.transparent,
                              child: Text(
                                Strings.dontHaveAccount,
                                style: Theme.of(context).textTheme.button,
                              ),
                            ),
                          ],
                        ),
                      );
                    })
              ],
            ),
          ),
          UiHelper.verticalSpace(16),
        ],
      ),
    );
  }
}

class ChangePasswordBottomSheet extends StatefulWidget {
  final String phone;

  const ChangePasswordBottomSheet({Key key, @required this.phone})
      : super(key: key);

  @override
  _ChangePasswordBottomSheetState createState() =>
      _ChangePasswordBottomSheetState();
}

class _ChangePasswordBottomSheetState extends State<ChangePasswordBottomSheet> {
  TextEditingController phoneNumbercontroller,
      codeController,
      passwordController;
  FocusNode phoneNumberFocusNode, otpFocusNode, passwordFocusNode;
  bool obscureText = true;

  @override
  void initState() {
    phoneNumbercontroller = TextEditingController(
      text: widget.phone,
    );
    codeController = TextEditingController();
    passwordController = TextEditingController();
    phoneNumberFocusNode = FocusNode();
    otpFocusNode = FocusNode();
    passwordFocusNode = FocusNode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelProvider<ForgotPasswordViewModel>.withConsumer(
      viewModel: ForgotPasswordViewModel(),
      builder: (context, model, _) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 42),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                Strings.forgotPassword,
                style: Theme.of(context).textTheme.button,
              ),
              UiHelper.verticalSpace(8),
              Text(
                Strings.forgotPasswordMsg,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.caption,
              ),
              UiHelper.verticalSpace(8),
              TextFormField(
                focusNode: phoneNumberFocusNode,
                controller: phoneNumbercontroller,
                keyboardType: TextInputType.number,
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(otpFocusNode);
                },
                style: Theme.of(context).textTheme.caption,
                decoration: InputDecoration(
                  hintStyle: Theme.of(context).textTheme.caption.apply(
                      color: ApplicationConstants.disabledLightThemeColor),
                  prefixIcon: Icon(
                    Icons.phone_android,
                  ),
                  hintText: Strings.phoneNumber,
                ),
              ),
              UiHelper.verticalSpace(16),
              TextFormField(
                focusNode: otpFocusNode,
                controller: codeController,
                keyboardType: TextInputType.number,
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(passwordFocusNode);
                },
                style: Theme.of(context).textTheme.caption,
                decoration: InputDecoration(
                  hintStyle: Theme.of(context).textTheme.caption.apply(
                      color: ApplicationConstants.disabledLightThemeColor),
                  prefixIcon: Icon(
                    Feather.edit,
                  ),
                  hintText: Strings.otpCode,
                ),
              ),
              UiHelper.verticalSpace(16),
              TextFormField(
                obscureText: obscureText,
                focusNode: passwordFocusNode,
                controller: passwordController,
                keyboardType: TextInputType.visiblePassword,
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                style: Theme.of(context).textTheme.caption,
                decoration: InputDecoration(
                  hintStyle: Theme.of(context).textTheme.caption.apply(
                      color: ApplicationConstants.disabledLightThemeColor),
                  prefixIcon: Icon(
                    Icons.lock_open,
                  ),
                  hintText: Strings.newPassword,
                  suffixIcon: Material(
                    type: MaterialType.transparency,
                    shape: CircleBorder(),
                    child: InkWell(
                      customBorder: CircleBorder(),
                      child: Icon(obscureText
                          ? Icons.visibility
                          : Icons.visibility_off),
                      onTap: () {
                        setState(() {
                          obscureText = !obscureText;
                        });
                      },
                    ),
                  ),
                ),
              ),
              UiHelper.verticalSpace(16),
              BusyButton(
                title: Strings.changePassword,
                busy: model.busy,
                onPressed: () {
                  model.confirmForgotPassword(phoneNumbercontroller.text,
                      codeController.text, passwordController.text);
                },
              ),
              UiHelper.verticalSpace(MediaQuery.of(context).viewInsets.bottom),
            ],
          ),
        );
      },
    );
  }
}

class ForgotPasswordBottomSheet extends StatelessWidget {
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ViewModelProvider<ForgotPasswordViewModel>.withConsumer(
      viewModel: ForgotPasswordViewModel(),
      builder: (context, ForgotPasswordViewModel model, _) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 42),
          child: Column(
            children: <Widget>[
              Text(
                Strings.forgotPassword,
                style: Theme.of(context).textTheme.button,
              ),
              UiHelper.verticalSpace(16),
              TextFormField(
                controller: _controller,
                keyboardType: TextInputType.number,
                style: Theme.of(context).textTheme.caption,
                decoration: InputDecoration(
                  hintStyle: Theme.of(context).textTheme.caption,
                  prefixIcon: Icon(
                    Icons.phone_android,
                  ),
                  hintText: Strings.phoneNumber,
                ),
              ),
              UiHelper.verticalSpace(16),
              BusyButton(
                title: Strings.requestNewPassword,
                busy: model.busy,
                onPressed: () {
                  model.forgotPassword(_controller.text).then((_) {
                    if (model.busy) {
                      _changePassword(context);
                      model.setBusy(false);
                    }
                  });
                },
              ),
              UiHelper.verticalSpace(MediaQuery.of(context).viewInsets.bottom),
            ],
          ),
        );
      },
    );
  }

  //change password
  _changePassword(context) {
    return UiHelper.showBottomSheet(
      context,
      child: ChangePasswordBottomSheet(
        phone: _controller.text,
      ),
    );
  }
}
