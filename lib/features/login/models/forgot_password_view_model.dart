import 'package:mcq/common/response_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/forgot_password_request_model.dart';
import 'package:mcq/common/services/authentication/repository/authentication_repository.dart';
import 'package:mcq/core/models/base_view_model.dart';
import 'package:mcq/core/models/failure.dart';
import 'package:mcq/core/services/dialog_service.dart';
import 'package:mcq/core/services/navigation_service.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/features/login/views/login_view.dart';

import '../../../locator.dart';

class ForgotPasswordViewModel extends BaseViewModel {
  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();

  UserDataService _userDataService = UserDataService();
  NavigationService _navigationService = locator<NavigationService>();
  DialogService _dialogService = locator<DialogService>();

  Future forgotPassword(String phoneNumber) async {
    setBusy(true);
    try {
      ResponseModel responseModel =
          await _authenticationRepository.forgotPassword(
              forgotPasswordRequestModel:
                  ForgotPasswordRequestModel(phone: phoneNumber.trim()));
      _dialogService.showDialog(
        title: "Forgot Password",
        description: responseModel.message,
        buttonTitle: "Ok",
      );
    } on Failure catch (e) {
      setBusy(false);
      _dialogService.showDialog(
        title: "Forgot Password",
        description: e.message,
        buttonTitle: "Ok",
      );
    }
  }

  Future confirmForgotPassword(
    String phoneNumber,
    String code,
    String password,
  ) async {
    setBusy(true);
    try {
      ResponseModel responseModel =
          await _authenticationRepository.confirmForgotPassword(
        forgotPasswordRequestModel: ForgotPasswordRequestModel(
          phone: phoneNumber.trim(),
          code: code.trim(),
          password: password.trim(),
        ),
      );
      _dialogService
          .showDialog(
        title: "Forgot Password",
        description: responseModel.message,
        buttonTitle: "Ok",
      )
          .then((_) {
        _userDataService.clearData();
        _navigationService.navigateTo(LoginView());
      });
      setBusy(false);
    } on Failure catch (e) {
      setBusy(false);
      _dialogService.showDialog(
        title: "Forgot Password",
        description: e.message,
        buttonTitle: "Ok",
      );
    }
  }
}
