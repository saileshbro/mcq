import 'package:mcq/common/services/authentication/authentication_data_source/models/login_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/login_response_model.dart';
import 'package:mcq/common/services/authentication/repository/authentication_repository.dart';
import 'package:mcq/core/models/base_view_model.dart';
import 'package:mcq/core/models/failure.dart';
import 'package:mcq/core/services/dialog_service.dart';
import 'package:mcq/core/services/navigation_service.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/dashboard/dashboard.dart';
import 'package:mcq/locator.dart';

class LoginViewModel extends BaseViewModel {
  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();

  UserDataService _userDataService = UserDataService();
  NavigationService _navigationService = locator<NavigationService>();
  DialogService _dialogService = locator<DialogService>();

  Future login(String phoneNumber, String password) async {
    setBusy(true);

    try {
      String _phoneNumber = phoneNumber.trim();
      String _password = password.trim();

      LoginResponseModel loginResponseModel =
          await _authenticationRepository.login(
              loginRequestModel:
                  LoginRequestModel(phone: _phoneNumber, password: _password));

      if (loginResponseModel.token != null) {
        _userDataService.saveData(
            loginResponseModel.token, loginResponseModel.name);

        print("Successful");
        setBusy(false);

        _navigationService.navigateTo(Dashboard(), clearStack: true);
////      TODO
//        return true;
      } else {
        throw Failure(message: "Some Internal Error");
      }
      return true;
    } on Failure catch (e) {
//      return false;
////      TODO
      _dialogService.showDialog(
        title: "Login Failure",
        description: e.message,
        buttonTitle: "Ok",
      );
      setBusy(false);
    }
  }
}
