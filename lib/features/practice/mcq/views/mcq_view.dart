import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:mcq/common/ui/busy_button.dart';
import 'package:mcq/common/ui/chips.dart';
import 'package:mcq/common/ui/section_app_bar.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/theme/theme_model.dart';

class McqView extends StatefulWidget {
  @override
  _McqViewState createState() => _McqViewState();
}

class _McqViewState extends State<McqView> {
  int selectedOptionNumber;
  List<int> skippedQuestions =
      List.generate(12, (int i) => Random().nextInt(100))
        ..sort((a, b) => a - b)
        ..toSet()
        ..toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage(
                ThemeModel.isDarkTheme(context)
                    ? 'assets/illustrations/practice_dark.png'
                    : 'assets/illustrations/practice_light.png',
              ),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CustomAppBar(),
              UiHelper.verticalSpace(20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Text(
                  "Skipped Questions",
                  style: Theme.of(context).textTheme.display3.copyWith(
                        color: ThemeModel.isDarkTheme(context)
                            ? ApplicationConstants.questionsDarkColor
                            : ApplicationConstants.primaryLightTextColor,
                      ),
                ),
              ),
              UiHelper.verticalSpace(15),
              Container(
                height: 32,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: skippedQuestions.length,
                  separatorBuilder: (BuildContext context, int index) {
                    return UiHelper.horizontalSpace(8);
                  },
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return Row(
                        children: <Widget>[
                          UiHelper.horizontalSpace(24),
                          Chips(
                            isSelected: false,
                            title: skippedQuestions[index].toString(),
                          ),
                        ],
                      );
                    }
                    if (index == skippedQuestions.length - 1) {
                      return Row(
                        children: <Widget>[
                          Chips(
                            isSelected: false,
                            title: skippedQuestions[index].toString(),
                          ),
                          UiHelper.horizontalSpace(24),
                        ],
                      );
                    }
                    return Chips(
                      isSelected: false,
                      title: skippedQuestions[index].toString(),
                    );
                  },
                ),
              ),
              UiHelper.verticalSpace(24),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              "Question 1",
                              style:
                                  Theme.of(context).textTheme.display3.copyWith(
                                        color: ThemeModel.isDarkTheme(context)
                                            ? ApplicationConstants
                                                .questionsDarkColor
                                            : ApplicationConstants
                                                .primaryLightTextColor,
                                      ),
                            ),
                            Text(
                              "/10",
                              style:
                                  Theme.of(context).textTheme.subtitle.copyWith(
                                        color: ThemeModel.isDarkTheme(context)
                                            ? ApplicationConstants
                                                .questionsDarkColor
                                            : ApplicationConstants
                                                .primaryLightTextColor,
                                      ),
                            ),
                          ],
                        ),
                        Text(
                          "18:05",
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                                color: ThemeModel.isDarkTheme(context)
                                    ? ApplicationConstants.questionsDarkColor
                                    : ApplicationConstants
                                        .primaryLightTextColor,
                              ),
                        )
                      ],
                    ),
                    UiHelper.verticalSpace(12),
                    _DashedLine(),
                    UiHelper.verticalSpace(12),
                    Text(
                      "This is the one and only question for mockup guys how is it actually ?",
                      style: Theme.of(context).textTheme.display3,
                    ),
                    UiHelper.verticalSpace(44),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 34),
                child: Column(
                  children: <Widget>[
                    OptionTile(
                      selectedOptionNumber: this.selectedOptionNumber,
                      onPressed: (int option) {
                        setState(() {
                          selectedOptionNumber = option;
                        });
                      },
                      option: "This is first answer",
                      optionNumber: 1,
                    ),
                    UiHelper.verticalSpace(16),
                    OptionTile(
                      selectedOptionNumber: this.selectedOptionNumber,
                      onPressed: (int option) {
                        setState(() {
                          selectedOptionNumber = option;
                        });
                      },
                      option: "This is first answer",
                      optionNumber: 2,
                    ),
                    UiHelper.verticalSpace(16),
                    OptionTile(
                      selectedOptionNumber: this.selectedOptionNumber,
                      onPressed: (int option) {
                        setState(() {
                          selectedOptionNumber = option;
                        });
                      },
                      option: "This is first answer",
                      optionNumber: 3,
                    ),
                    UiHelper.verticalSpace(16),
                    OptionTile(
                      selectedOptionNumber: this.selectedOptionNumber,
                      onPressed: (int option) {
                        setState(() {
                          selectedOptionNumber = option;
                        });
                      },
                      option: "This is first answer",
                      optionNumber: 4,
                    ),
                    UiHelper.verticalSpace(34),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 66),
                      child: BusyButton(
                        title: Strings.next,
                        onPressed: () {},
                      ),
                    ),
                    UiHelper.verticalSpace(16),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    Strings.confused,
                    style: Theme.of(context).textTheme.subtitle.copyWith(
                          color: ThemeModel.isDarkTheme(context)
                              ? ApplicationConstants.questionsDarkColor
                              : ApplicationConstants.primaryLightTextColor,
                        ),
                  ),
                  UiHelper.horizontalSpace(4),
                  Text(
                    Strings.skipForNow,
                    style: Theme.of(context).textTheme.subtitle.copyWith(
                          color: ApplicationConstants.primaryColor,
                        ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class OptionTile extends StatelessWidget {
  final String option;
  final Function(int selectedOptionNumber) onPressed;
  final int optionNumber;
  final int selectedOptionNumber;

  const OptionTile({
    Key key,
    @required this.option,
    @required this.onPressed,
    @required this.optionNumber,
    @required this.selectedOptionNumber,
  }) : super(key: key);

  bool get isSelected => optionNumber == selectedOptionNumber;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onPressed(this.optionNumber);
      },
      borderRadius: BorderRadius.circular(16),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          border: Border.all(
            width: 2,
            color: ApplicationConstants.optionDarkBorderColor,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              option,
              style: Theme.of(context).textTheme.subtitle,
            ),
            Container(
              height: 24,
              width: 24,
              decoration: !isSelected
                  ? BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        width: 2,
                        color: ApplicationConstants.optionDarkBorderColor,
                      ),
                    )
                  : BoxDecoration(),
              child: isSelected
                  ? Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: ApplicationConstants.primaryColor),
                      child: Center(
                        child: Icon(
                          Icons.check,
                          size: 20,
                          color: ThemeModel.isDarkTheme(context)
                              ? Colors.black
                              : Colors.white,
                        ),
                      ),
                    )
                  : SizedBox.shrink(),
            )
          ],
        ),
      ),
    );
  }
}

class _DashedLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 6.0;
        final dashHeight = 2.0;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: ApplicationConstants.disabledLightThemeColor,
                ),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
