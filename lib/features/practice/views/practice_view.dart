import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/common/ui/course_tile.dart';
import 'package:mcq/common/ui/custom_popup_menu_button.dart';
import 'package:mcq/common/ui/gap.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/navigation_tile.dart';
import 'package:mcq/common/ui/section_app_bar.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/core/services/navigation_service.dart';
import 'package:mcq/features/practice/mcq/views/mcq_view.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/locator.dart';

class PracticeView extends StatefulWidget {
  final HomeTileModel homeTileModel;

  const PracticeView({Key key, @required this.homeTileModel}) : super(key: key);

  @override
  _PracticeViewState createState() => _PracticeViewState();
}

class _PracticeViewState extends State<PracticeView> {
  final NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SectionAppBar(
              homeTileModel: widget.homeTileModel,
              curvedButtonChild: CustomPopupMenuButton(),
            ),
            UiHelper.verticalSpace(24),
            NavigationTile(
              onPressed: () {},
              color: ApplicationConstants.videosColor,
              gradientColor: ApplicationConstants.videosGradientColor,
              title: Strings.performanceHistory,
              icon: Feather.video,
            ),
            NavigationTile(
              onPressed: () {},
              color: ApplicationConstants.examsColor,
              gradientColor: ApplicationConstants.examsGradientColor,
              title: Strings.highestScores,
              icon: Feather.edit_3,
            ),
            UiHelper.verticalSpace(12),
            Gap(),
            UiHelper.verticalSpace(24),
            Padding(
              padding: const EdgeInsets.only(left: 24.0),
              child: Text(
                Strings.practiceSets,
                style: Theme.of(context).textTheme.display3,
              ),
            ),
            ..._getCourses()
          ],
        ),
      ),
    );
  }

  List<CustomListTile> _getCourses() {
    return [
      CustomListTile(
        title: "Practice Set 1",
        onTap: () {
          _navigationService.navigateTo(McqView());
        },
        imageUrl: "",
        subTitle: "45 Questions",
        loaded: true,
      ),
      CustomListTile(
        title: "Practice Set 1",
        onTap: () {
          _navigationService.navigateTo(McqView());
        },
        imageUrl: "",
        subTitle: "45 Questions",
        loaded: true,
      ),
      CustomListTile(
        title: "Practice Set 1",
        onTap: () {
          _navigationService.navigateTo(McqView());
        },
        imageUrl: "",
        subTitle: "45 Questions",
        loaded: true,
      ),
      CustomListTile(
        title: "Practice Set 1",
        onTap: () {
          _navigationService.navigateTo(McqView());
        },
        imageUrl: "",
        subTitle: "45 Questions",
        loaded: true,
      ),
      CustomListTile(
        title: "Practice Set 1",
        onTap: () {
          _navigationService.navigateTo(McqView());
        },
        imageUrl: "",
        subTitle: "45 Questions",
        loaded: true,
      ),
      CustomListTile(
        title: "Practice Set 1",
        onTap: () {
          _navigationService.navigateTo(McqView());
        },
        imageUrl: "",
        subTitle: "45 Questions",
        loaded: true,
      ),
    ];
  }
}
