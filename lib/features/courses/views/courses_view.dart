import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/common/services/home/home_data_source/models/course_respoonse_model.dart';
import 'package:mcq/common/ui/chips.dart';
import 'package:mcq/common/ui/navbaritem_appbar.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/core/services/navigation_service.dart';
import 'package:mcq/features/courses/subject/views/subject_view.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/locator.dart';
import 'package:mcq/theme/theme_model.dart';

class CourseView extends StatefulWidget {
  @override
  _CourseViewState createState() => _CourseViewState();
}

class _CourseViewState extends State<CourseView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          NavBarItemAppBar(
            navBarItemTitle: Strings.courses,
            blackString: Strings.chooseYour,
            greenString: Strings.rightAway,
          ),
          UiHelper.verticalSpace(10),
          Container(
            padding: EdgeInsets.only(left: 24),
            child: Text(
              Strings.enrolledCourses,
              style: Theme.of(context).textTheme.display3,
            ),
          ),
          UiHelper.verticalSpace(16),
          Container(
            height: MediaQuery.of(context).size.width * .60,
            child: ListView(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              children: [
                for (int i = 0; i <= courses.length; i++)
                  if (i == courses.length)
                    UiHelper.horizontalSpace(24)
                  else
                    _EnrollCoursesItem(
                      enrolledCourse: courses[i],
                    )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 24),
            child: Text(
              Strings.availableCourses,
              style: Theme.of(context).textTheme.display3,
            ),
          ),
          UiHelper.verticalSpace(16),
          Container(
            height: 32,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                UiHelper.horizontalSpace(20),
                Chips(title: 'All', isSelected: true),
                Chips(title: 'IOE', isSelected: false),
                Chips(title: 'MOE', isSelected: false),
                Chips(title: 'NEB', isSelected: false),
                Chips(title: 'Lok Sewa', isSelected: false),
                Chips(title: 'IIC', isSelected: false),
                Chips(title: 'IELTS', isSelected: false),
                Chips(title: 'TOFEL', isSelected: false),
                Chips(title: 'GRE', isSelected: false),
              ],
            ),
          ),
          for (int i = 0; i < courses.length; i++)
            if (i == courses.length - 1)
              _CoursesItem(
                course: courses[i],
              )
            else ...[
              _CoursesItem(
                course: courses[i],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Container(
                    height: 1,
                    color: ThemeModel.isDarkTheme(context)
                        ? ApplicationConstants.disabledDarkThemeColor
                        : ApplicationConstants.gapColor),
              )
            ]
        ],
      ),
    );
  }
}

class _EnrollCoursesItem extends StatelessWidget {
  final Course enrolledCourse;
  final bool loaded;

  const _EnrollCoursesItem({
    Key key,
    @required this.enrolledCourse,
    this.loaded = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(24, 20, 0, 24),
      width: MediaQuery.of(context).size.width * .8,
      decoration: BoxDecoration(
        color: ThemeModel.isDarkTheme(context)
            ? ApplicationConstants.primaryMediumTextColor
            : Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: loaded
            ? [
                !ThemeModel.isDarkTheme(context)
                    ? BoxShadow(
                        offset: Offset(0, 4),
                        blurRadius: 20,
                        color:
                            ApplicationConstants.primaryColor.withOpacity(0.37),
                      )
                    : BoxShadow(
                        blurRadius: 15,
                        color: Color(0xff000000).withOpacity(0.2),
                      ),
              ]
            : [
                BoxShadow(
                  blurRadius: 15,
                  color: Color(0xff000000).withOpacity(0.2),
                )
              ],
      ),
      // height: ,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: SizedBox.expand(
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    topLeft: Radius.circular(8)),
                child: loaded
                    ? Image.asset(
                        enrolledCourse.picture,
                        height: MediaQuery.of(context).size.width * .4 + 60,
                        fit: BoxFit.fill,
                      )
                    : UiHelper.shimmerContainer(double.infinity,
                        MediaQuery.of(context).size.width * .4 + 60),
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              width: MediaQuery.of(context).size.width * .4,
              padding: EdgeInsets.all(16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  loaded
                      ? Container(
                          width: double.infinity,
                          height: 24,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: ApplicationConstants.primaryColor,
                          ),
                          child: Center(
                            child: InkWell(
                                child: Text(
                                  enrolledCourse.price,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle
                                      .copyWith(
                                        color: ApplicationConstants
                                            .textFieldBackgroundColor,
                                      ),
                                ),
                                onTap: () {
                                  print('button pressed');
                                }),
                          ),
                        )
                      : UiHelper.shimmerContainer(double.infinity, 24,
                          radius: 8),
                  UiHelper.verticalSpace(11),
                  loaded
                      ? Text(
                          enrolledCourse.name,
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                              color: !ThemeModel.isDarkTheme(context)
                                  ? ApplicationConstants.primaryMediumTextColor
                                  : ApplicationConstants
                                      .textFieldBackgroundColor),
                        )
                      : UiHelper.shimmerContainer(100, 16),
                  UiHelper.verticalSpace(11),
                  loaded
                      ? Text(
                          enrolledCourse.courseDescription,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                          style: Theme.of(context).textTheme.caption.copyWith(
                              color: !ThemeModel.isDarkTheme(context)
                                  ? ApplicationConstants.primaryLightTextColor
                                  : ApplicationConstants
                                      .disabledLightThemeColor),
                        )
                      : UiHelper.shimmerContainer(double.infinity, 65),
                  Spacer(),
                  loaded
                      ? Container(
                          height: 28,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: ThemeModel.isDarkTheme(context)
                                  ? ApplicationConstants.disabledDarkThemeColor
                                  : ApplicationConstants.gapColor,
                              borderRadius: BorderRadius.circular(8)),
                          child: InkWell(
                              child: Icon(
                                Entypo.arrow_long_right,
                                size: 18,
                                color: !ThemeModel.isDarkTheme(context)
                                    ? ApplicationConstants
                                        .primaryMediumTextColor
                                    : ApplicationConstants
                                        .textFieldBackgroundColor,
                              ),
                              onTap: () {
                                print('button pressed');
                              }),
                        )
                      : UiHelper.shimmerContainer(double.infinity, 28,
                          radius: 8),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _CoursesItem extends StatelessWidget {
  final Course course;
  final bool loaded;

  const _CoursesItem({
    Key key,
    @required this.course,
    this.loaded = true,
  });

  @override
  Widget build(BuildContext context) {
    final NavigationService _navigationService = locator<NavigationService>();
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(24),
      child: Row(
        children: <Widget>[
          loaded
              ? Container(
                  height: 88,
                  width: 80,
                  decoration: BoxDecoration(
                      boxShadow: UiHelper.getBoxShadow(
                          context, ApplicationConstants.primaryColor),
                      borderRadius: BorderRadius.circular(8)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Image.asset(
                      this.course.picture,
                      fit: BoxFit.cover,
                    ),
                  ))
              : UiHelper.shimmerContainer(80, 88, radius: 8),
          UiHelper.horizontalSpace(16),
          Flexible(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  loaded
                      ? Text(
                          this.course.name,
                          style: Theme.of(context).textTheme.subtitle,
                        )
                      : UiHelper.shimmerContainer(100, 16),
                  UiHelper.verticalSpace(10),
                  Row(
                    children: <Widget>[
                      loaded
                          ? Container(
                              width: 80,
                              height: 20,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: ApplicationConstants.colorPrice),
                              child: Center(
                                child: Text(
                                  this.course.price,
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption
                                      .copyWith(
                                          color: ApplicationConstants
                                              .textFieldBackgroundColor),
                                ),
                              ),
                            )
                          : UiHelper.shimmerContainer(80, 20, radius: 20),
                      UiHelper.horizontalSpace(16),
                      loaded
                          ? Text(this.course.contentDescription,
                              style: Theme.of(context).textTheme.caption)
                          : UiHelper.shimmerContainer(100, 16)
                    ],
                  ),
                  UiHelper.verticalSpace(10),
                  loaded
                      ? Container(
                          height: 28,
                          decoration: BoxDecoration(
                              color: ThemeModel.isDarkTheme(context)
                                  ? ApplicationConstants.disabledDarkThemeColor
                                  : ApplicationConstants.gapColor,
                              borderRadius: BorderRadius.circular(8)),
                          child: FlatButton(
                            onPressed: () {
                              _navigationService.navigateTo(SubjectView());
                            },
                            child: course.price.toLowerCase() == 'free'
                                ? Center(
                                    child: Icon(Entypo.arrow_long_right,
                                        size: 18,
                                        color: ThemeModel.isDarkTheme(context)
                                            ? ApplicationConstants.gapColor
                                            : ApplicationConstants
                                                .primaryLightTextColor),
                                  )
                                : Center(
                                    child: Text(
                                      Strings.enroll,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle
                                          .copyWith(
                                            color:
                                                ThemeModel.isDarkTheme(context)
                                                    ? ApplicationConstants
                                                        .gapColor
                                                    : ApplicationConstants
                                                        .primaryLightTextColor,
                                          ),
                                    ),
                                  ),
                          ))
                      : Center(
                          child: UiHelper.shimmerContainer(double.infinity, 28,
                              radius: 8),
                        ),
                ]),
          ),
        ],
      ),
    );
  }
}

final List<Course> courses = [
  Course(
    picture: 'assets/illustrations/course.png',
    contentDescription: "20+ Lessons",
    courseDescription:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla ratione veritatis illum. Nobis, sapiente qui ipsum pariatur perferendis amet maiores expedita aliquam porro et eligendi vel, aut temporibus maxime voluptatibus distinctio commodi, assumenda fugit laboriosam! Suscipit fuga eos vitae corporis animi incidunt porro quia non tenetur a voluptate at autem dignissimos, accusamus similique nisi et mollitia harum facere corrupti rem! Fugiat veniam animi iusto asperiores eius vel ipsa? Illum maxime ipsam enim fugit, alias ullam sapiente at, assumenda eligendi debitis cum! Voluptates repellat, temporibus a numquam amet ex placeat aut! Sunt officia libero tenetur praesentium dolor, ea deleniti perferendis earum quasi corrupti eligendi, nisi nesciunt, autem minima. Laudantium minima exercitationem dignissimos vero provident dolores consectetur debitis, natus distinctio possimus reiciendis recusandae voluptatibus aut officiis quis maxime. Minima maiores inventore, praesentium maxime suscipit, autem perferendis facilis reprehenderit atque natus quia sed voluptatibus expedita dolores corrupti architecto veritatis voluptatum animi ipsam! Tenetur!",
    price: "Rs. 600",
    name: Strings.physics,
  ),
  Course(
    picture: 'assets/illustrations/course.png',
    contentDescription: "12+ Lessons",
    courseDescription:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla ratione veritatis illum. Nobis, sapiente qui ipsum pariatur perferendis amet maiores expedita aliquam porro et eligendi vel, aut temporibus maxime voluptatibus distinctio commodi, assumenda fugit laboriosam! Suscipit fuga eos vitae corporis animi incidunt porro quia non tenetur a voluptate at autem dignissimos, accusamus similique nisi et mollitia harum facere corrupti rem! Fugiat veniam animi iusto asperiores eius vel ipsa? Illum maxime ipsam enim fugit, alias ullam sapiente at, assumenda eligendi debitis cum! Voluptates repellat, temporibus a numquam amet ex placeat aut! Sunt officia libero tenetur praesentium dolor, ea deleniti perferendis earum quasi corrupti eligendi, nisi nesciunt, autem minima. Laudantium minima exercitationem dignissimos vero provident dolores consectetur debitis, natus distinctio possimus reiciendis recusandae voluptatibus aut officiis quis maxime. Minima maiores inventore, praesentium maxime suscipit, autem perferendis facilis reprehenderit atque natus quia sed voluptatibus expedita dolores corrupti architecto veritatis voluptatum animi ipsam! Tenetur!",
    price: "Free",
    name: Strings.mathematics,
  ),
  Course(
    picture: 'assets/illustrations/course.png',
    contentDescription: "12+ Lessons",
    courseDescription:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla ratione veritatis illum. Nobis, sapiente qui ipsum pariatur perferendis amet maiores expedita aliquam porro et eligendi vel, aut temporibus maxime voluptatibus distinctio commodi, assumenda fugit laboriosam! Suscipit fuga eos vitae corporis animi incidunt porro quia non tenetur a voluptate at autem dignissimos, accusamus similique nisi et mollitia harum facere corrupti rem! Fugiat veniam animi iusto asperiores eius vel ipsa? Illum maxime ipsam enim fugit, alias ullam sapiente at, assumenda eligendi debitis cum! Voluptates repellat, temporibus a numquam amet ex placeat aut! Sunt officia libero tenetur praesentium dolor, ea deleniti perferendis earum quasi corrupti eligendi, nisi nesciunt, autem minima. Laudantium minima exercitationem dignissimos vero provident dolores consectetur debitis, natus distinctio possimus reiciendis recusandae voluptatibus aut officiis quis maxime. Minima maiores inventore, praesentium maxime suscipit, autem perferendis facilis reprehenderit atque natus quia sed voluptatibus expedita dolores corrupti architecto veritatis voluptatum animi ipsam! Tenetur!",
    price: "Free",
    name: Strings.mathematics,
  ),
  Course(
    picture: 'assets/illustrations/course.png',
    contentDescription: "12+ Lessons",
    courseDescription:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla ratione veritatis illum. Nobis, sapiente qui ipsum pariatur perferendis amet maiores expedita aliquam porro et eligendi vel, aut temporibus maxime voluptatibus distinctio commodi, assumenda fugit laboriosam! Suscipit fuga eos vitae corporis animi incidunt porro quia non tenetur a voluptate at autem dignissimos, accusamus similique nisi et mollitia harum facere corrupti rem! Fugiat veniam animi iusto asperiores eius vel ipsa? Illum maxime ipsam enim fugit, alias ullam sapiente at, assumenda eligendi debitis cum! Voluptates repellat, temporibus a numquam amet ex placeat aut! Sunt officia libero tenetur praesentium dolor, ea deleniti perferendis earum quasi corrupti eligendi, nisi nesciunt, autem minima. Laudantium minima exercitationem dignissimos vero provident dolores consectetur debitis, natus distinctio possimus reiciendis recusandae voluptatibus aut officiis quis maxime. Minima maiores inventore, praesentium maxime suscipit, autem perferendis facilis reprehenderit atque natus quia sed voluptatibus expedita dolores corrupti architecto veritatis voluptatum animi ipsam! Tenetur!",
    price: "Free",
    name: Strings.mathematics,
  ),
  Course(
    picture: 'assets/illustrations/course.png',
    contentDescription: "34+ Lessons",
    courseDescription:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla ratione veritatis illum. Nobis, sapiente qui ipsum pariatur perferendis amet maiores expedita aliquam porro et eligendi vel, aut temporibus maxime voluptatibus distinctio commodi, assumenda fugit laboriosam! Suscipit fuga eos vitae corporis animi incidunt porro quia non tenetur a voluptate at autem dignissimos, accusamus similique nisi et mollitia harum facere corrupti rem! Fugiat veniam animi iusto asperiores eius vel ipsa? Illum maxime ipsam enim fugit, alias ullam sapiente at, assumenda eligendi debitis cum! Voluptates repellat, temporibus a numquam amet ex placeat aut! Sunt officia libero tenetur praesentium dolor, ea deleniti perferendis earum quasi corrupti eligendi, nisi nesciunt, autem minima. Laudantium minima exercitationem dignissimos vero provident dolores consectetur debitis, natus distinctio possimus reiciendis recusandae voluptatibus aut officiis quis maxime. Minima maiores inventore, praesentium maxime suscipit, autem perferendis facilis reprehenderit atque natus quia sed voluptatibus expedita dolores corrupti architecto veritatis voluptatum animi ipsam! Tenetur!",
    price: "Rs. 60",
    name: Strings.physics,
  )
];
