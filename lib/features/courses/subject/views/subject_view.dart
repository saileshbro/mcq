import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mcq/common/ui/course_section_app_bar.dart';
import 'package:mcq/common/ui/course_tile.dart';
import 'package:mcq/common/ui/course_tile_model.dart';
import 'package:mcq/common/ui/gap.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/navigation_tile.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/core/services/navigation_service.dart';
import 'package:mcq/features/notes/views/note_view.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/locator.dart';

class SubjectView extends StatefulWidget {
  @override
  _SubjectViewState createState() => _SubjectViewState();
}

class _SubjectViewState extends State<SubjectView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          CourseSectionAppBar(
            courseTileModel: courseTiles[0],
          ),
          UiHelper.verticalSpace(10),
          _SubjectBody(
            subjectTitle: courseTiles[0].title,
          ),
        ],
      ),
    );
  }
}

class _SubjectBody extends StatelessWidget {
  final String subjectTitle;
  final NavigationService _navigationService = locator<NavigationService>();

  _SubjectBody({Key key, @required this.subjectTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        UiHelper.verticalSpace(16),
        Container(
          padding: EdgeInsets.only(left: 24.0),
          child: RichText(
              text: TextSpan(
            children: [
              TextSpan(
                text: "Courses tags : ",
                style: Theme.of(context).textTheme.caption,
              ),
              TextSpan(
                text: "IOE, MOE",
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(color: ApplicationConstants.primaryColor),
              ),
            ],
          )),
        ),
        UiHelper.verticalSpace(10),
        Column(
          children: <Widget>[
            NavigationTile(
              onPressed: () {},
              color: ApplicationConstants.videosColor,
              icon: Feather.video,
              gradientColor: ApplicationConstants.videosGradientColor,
              title: Strings.videos,
            ),
            NavigationTile(
              onPressed: () {},
              color: ApplicationConstants.practiceColor,
              icon: Feather.edit_3,
              gradientColor: ApplicationConstants.practiceGradientColor,
              title: Strings.practice + " MCQ's",
            ),
            NavigationTile(
              onPressed: () {
                _navigationService.navigateTo(
                  NoteView(
                    homeTileModel: HomeTileModel(
                      tileName: subjectTitle,
                      color: ApplicationConstants.notesColor,
                      icon: Feather.file_text,
                      gradientColor: ApplicationConstants.notesGradientColor,
                    ),
                  ),
                );
              },
              color: ApplicationConstants.notesColor,
              icon: Feather.file_text,
              gradientColor: ApplicationConstants.notesGradientColor,
              title: Strings.notes,
            ),
            NavigationTile(
              onPressed: () {},
              color: ApplicationConstants.calenderColor,
              icon: Feather.book_open,
              gradientColor: ApplicationConstants.calenderGradientColor,
              title: Strings.importantFormula,
            ),
          ],
        ),
        UiHelper.verticalSpace(16),
        Gap(),
        UiHelper.verticalSpace(16),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Text(
                Strings.courseNotes,
                style: Theme.of(context).textTheme.display3,
              ),
            ),
            ...[
              CustomListTile(
                imageUrl: 'assets/icons/physics.png',
                title: "Mechanics",
                subTitle: "400+ course content",
                onTap: () {},
                loaded: true,
              ),
              CustomListTile(
                imageUrl: 'assets/icons/physics.png',
                title: "Light and sound",
                subTitle: "400+ course content",
                onTap: () {},
                loaded: true,
              ),
              CustomListTile(
                imageUrl: 'assets/icons/physics.png',
                title: "Electrostatics",
                subTitle: "400+ course content",
                onTap: () {},
                loaded: true,
              ),
              CustomListTile(
                imageUrl: 'assets/icons/physics.png',
                title: "Magnetism",
                subTitle: "400+ course content",
                borderRequired: true,
                onTap: () {},
              ),
              CustomListTile(
                imageUrl: 'assets/icons/physics.png',
                title: "Mechanics",
                subTitle: "400+ course content",
                onTap: () {},
                loaded: true,
              ),
              CustomListTile(
                imageUrl: 'assets/icons/physics.png',
                title: "Light and sound",
                subTitle: "400+ course content",
                onTap: () {},
                loaded: true,
              ),
              CustomListTile(
                imageUrl: 'assets/icons/physics.png',
                title: "Electrostatics",
                subTitle: "400+ course content",
                onTap: () {},
                loaded: true,
              ),
              CustomListTile(
                imageUrl: 'assets/icons/physics.png',
                title: "Magnetism",
                subTitle: "400+ course content",
                borderRequired: true,
                onTap: () {},
              ),
            ]
          ],
        )
      ],
    );
  }
}

List<CourseTileModel> courseTiles = [
  CourseTileModel(
      title: Strings.physics,
      imgUrl:
          "https://upload.wikimedia.org/wikipedia/commons/e/e6/Keerthy_Suresh_at_Thodari_Audio_Launch.jpg"),
];
