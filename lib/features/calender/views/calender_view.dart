import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:mcq/common/ui/custom_popup_menu_button.dart';
import 'package:mcq/common/ui/gap.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/section_app_bar.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/theme/theme_model.dart';

class CalenderView extends StatelessWidget {
  final HomeTileModel homeTileModel;

  CalenderView({@required this.homeTileModel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: <Widget>[
          SectionAppBar(
            homeTileModel: homeTileModel,
            curvedButtonChild: CustomPopupMenuButton(),
          ),
          UiHelper.verticalSpace(24),
          CalenderWidget(),
//          UiHelper.verticalSpace(24),
          Gap()
        ],
      ),
    );
  }
}

class CalenderWidget extends StatefulWidget {
  final ThemeModel model;

  const CalenderWidget({Key key, this.model}) : super(key: key);

  @override
  _CalenderWidgetState createState() => _CalenderWidgetState();
}

class _CalenderWidgetState extends State<CalenderWidget> {
  DateTime selectedDate;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24.0),
      child: CalendarCarousel<Event>(
        childAspectRatio: 1.267,
        customGridViewPhysics: NeverScrollableScrollPhysics(),
        pageScrollPhysics: BouncingScrollPhysics(),
        pageSnapping: true,
        isScrollable: true,
        todayButtonColor: ApplicationConstants.primaryColor,
        todayBorderColor: ApplicationConstants.primaryColor,
        todayTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ApplicationConstants.textFieldBackgroundColor,
            ),
        height: MediaQuery.of(context).size.height * 0.48,
        daysTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.textFieldBackgroundColor
                  : ApplicationConstants.primaryMediumTextColor,
            ),
        dayPadding: 0,
        daysHaveCircularBorder: true,
        headerTextStyle: Theme.of(context).textTheme.subtitle.copyWith(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.textFieldBackgroundColor
                  : ApplicationConstants.primaryMediumTextColor,
            ),
        weekdayTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.textFieldBackgroundColor
                  : ApplicationConstants.primaryMediumTextColor,
            ),
        weekendTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.textFieldBackgroundColor
                  : ApplicationConstants.primaryMediumTextColor,
            ),
        selectedDayTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ApplicationConstants.textFieldBackgroundColor,
            ),
        onDayPressed: (DateTime dateTime, _) {
          setState(() {
            selectedDate = dateTime;
          });
        },
        iconColor: ThemeModel.isDarkTheme(context)
            ? ApplicationConstants.textFieldBackgroundColor
            : ApplicationConstants.primaryMediumTextColor,
        selectedDateTime: selectedDate,
        selectedDayBorderColor: Colors.transparent,
        selectedDayButtonColor:
            ApplicationConstants.primaryColor.withOpacity(0.65),
        inactiveDaysTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.primaryLightTextColor
                  : ApplicationConstants.disabledLightThemeColor,
            ),
        inactiveWeekendTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.primaryLightTextColor
                  : ApplicationConstants.disabledLightThemeColor,
            ),
        nextDaysTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.primaryLightTextColor
                  : ApplicationConstants.disabledLightThemeColor,
            ),
        prevDaysTextStyle: Theme.of(context).textTheme.caption.copyWith(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.primaryLightTextColor
                  : ApplicationConstants.disabledLightThemeColor,
            ),
        markedDateIconBuilder: (Event e) {
          return e.icon;
        },
        markedDateShowIcon: true,
        markedDatesMap: EventList<Event>(
          events: {
            new DateTime(2020, 1, 26): [
              new Event(
                date: new DateTime(2020, 1, 26),
                title: 'Event 1',
                dot: Positioned(
                    child:
                        Container(color: Colors.red, height: 4.0, width: 4.0),
                    bottom: 14.0,
                    left: 18.0),
              ),
              new Event(
                date: new DateTime(2020, 1, 27),
                title: 'Event 2',
                dot: Positioned(
                    child:
                        Container(color: Colors.red, height: 4.0, width: 4.0),
                    bottom: 14.0,
                    left: 18.0),
              ),
              new Event(
                date: new DateTime(2020, 1, 28),
                title: 'Event 3',
                dot: Positioned(
                    child:
                        Container(color: Colors.red, height: 4.0, width: 4.0),
                    bottom: 14.0,
                    left: 18.0),
              )
            ],
          },
        ),
        markedDateIconMaxShown: 1,
        markedDateWidget: Positioned(
            child: Container(color: Colors.red, height: 4.0, width: 4.0),
            bottom: 14.0,
            left: 18.0),
      ),
    );
  }
}
