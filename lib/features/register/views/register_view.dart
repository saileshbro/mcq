import 'package:flutter/material.dart';
import 'package:mcq/common/ui/busy_button.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/features/register/models/register_view_model.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:provider_architecture/viewmodel_provider.dart';

class RegisterView extends StatefulWidget {
  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  bool obscureText = true;

  FocusNode nameFocusNode;
  FocusNode phoneFocusNode;
  FocusNode passwordFocusNode;

  TextEditingController _phoneController, _nameController, _passwordController;

  @override
  void initState() {
    super.initState();
    nameFocusNode = FocusNode();
    phoneFocusNode = FocusNode();
    passwordFocusNode = FocusNode();

    _phoneController = TextEditingController();
    _nameController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Navigator.canPop(context) ? AppBar() : SizedBox.shrink(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32),
                  child: Image.asset(
                    ApplicationConstants.registerIllustration,
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
                UiHelper.verticalSpace(20),
                Text(
                  Strings.register,
                  style: Theme.of(context).textTheme.display4,
                ),
                UiHelper.verticalSpace(26),
                ViewModelProvider<RegisterViewModel>.withConsumer(
                    viewModel: RegisterViewModel(),
                    builder: (context, model, child) {
                      return Form(
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              controller: _nameController,
                              style: Theme.of(context).textTheme.caption,
                              textCapitalization: TextCapitalization.words,
                              keyboardType: TextInputType.text,
                              focusNode: nameFocusNode,
                              onEditingComplete: () {
                                FocusScope.of(context)
                                    .requestFocus(phoneFocusNode);
                              },
                              decoration: InputDecoration(
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                        color: ApplicationConstants
                                            .disabledLightThemeColor),
                                prefixIcon: Icon(
                                  Icons.perm_identity,
                                ),
                                hintText: Strings.fullName,
                              ),
                            ),
                            UiHelper.verticalSpace(16),
                            TextFormField(
                              controller: _phoneController,
                              style: Theme.of(context).textTheme.caption,
                              focusNode: phoneFocusNode,
                              onEditingComplete: () {
                                FocusScope.of(context)
                                    .requestFocus(passwordFocusNode);
                              },
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                        color: ApplicationConstants
                                            .disabledLightThemeColor),
                                prefixIcon: Icon(
                                  Icons.phone_android,
                                ),
                                hintText: Strings.phoneNumber,
                              ),
                            ),
                            UiHelper.verticalSpace(16),
                            TextFormField(
                              controller: _passwordController,
                              style: Theme.of(context).textTheme.caption,
                              focusNode: passwordFocusNode,
                              onEditingComplete: () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                              },
                              obscureText: obscureText,
                              decoration: InputDecoration(
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                        color: ApplicationConstants
                                            .disabledLightThemeColor),
                                prefixIcon: Icon(
                                  Icons.lock_open,
                                ),
                                suffixIcon: Material(
                                  type: MaterialType.transparency,
                                  shape: CircleBorder(),
                                  child: InkWell(
                                    customBorder: CircleBorder(),
                                    child: Icon(obscureText
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                    onTap: () {
                                      setState(() {
                                        obscureText = !obscureText;
                                      });
                                    },
                                  ),
                                ),
                                hintText: Strings.password,
                              ),
                            ),
                            UiHelper.verticalSpace(28),
                            Container(
                              decoration: BoxDecoration(
                                boxShadow: UiHelper.getBoxShadow(
                                  context,
                                  ApplicationConstants.primaryColor,
                                ),
                              ),
                              width: double.infinity,
                              child: BusyButton(
//                                color: ApplicationConstants.primaryColor,
                                title: (Strings.register),
                                busy: model.busy,
                                onPressed: model.busy
                                    ? null
                                    : () {
                                        model.register(
                                          _nameController.text,
                                          _phoneController.text,
                                          _passwordController.text,
                                        );
                                      },
                              ),
                            ),
                            // UiHelper.verticalSpace(16),
                            FlatButton(
                              onPressed: () {},
                              splashColor: Colors.transparent,
                              child: Text(
                                Strings.alreadyHaveAnAccount,
                                style: Theme.of(context).textTheme.button,
                              ),
                            ),
                          ],
                        ),
                      );
                    })
              ],
            ),
          ),
          UiHelper.verticalSpace(16),
        ],
      ),
    );
  }
}
