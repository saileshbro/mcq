import 'package:flutter/material.dart';
import 'package:mcq/common/ui/busy_button.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/constants/strings.dart';
import 'package:mcq/features/register/models/resend_verification_view_model.dart';
import 'package:mcq/features/register/models/verification_view_model.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/theme/theme_model.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';
import 'package:provider_architecture/viewmodel_provider.dart';

class VerificationView extends StatefulWidget {
  final String phoneNumber;

  const VerificationView({Key key, @required this.phoneNumber})
      : super(key: key);

  @override
  _VerificationViewState createState() => _VerificationViewState();
}

class _VerificationViewState extends State<VerificationView> {
  String pinCodeValue = '';
  TextEditingController pinEditingController;

  @override
  void initState() {
    pinEditingController = TextEditingController(
      text: pinCodeValue,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          AppBar(
//            brightness: Brightness.light,
              ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32),
                  child: Image.asset(
                    ApplicationConstants.verificationIllustration,
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
              ],
            ),
          ),
          UiHelper.verticalSpace(20),
          Text(
            Strings.verification,
            style: Theme.of(context).textTheme.display4,
            textAlign: TextAlign.center,
          ),
          UiHelper.verticalSpace(20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 75.0),
            child: Text(
              Strings.enterTheVerification,
              style: Theme.of(context).textTheme.subtitle,
              textAlign: TextAlign.center,
            ),
          ),
          UiHelper.verticalSpace(22),
          ViewModelProvider<VerificationViewModel>.withConsumer(
            viewModel: VerificationViewModel(),
            builder: (BuildContext context, VerificationViewModel model, _) {
              return Form(
                child: Column(
                  children: <Widget>[
                    Theme(
                      data: ThemeData(
                        inputDecorationTheme: InputDecorationTheme(
                          filled: true,
                          fillColor: Theme.of(context).scaffoldBackgroundColor,
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 80),
                        child: PinInputTextField(
                          pinLength: 4,
                          onChanged: (String value) {
                            setState(() {
                              pinCodeValue = value;
                            });
                          },
                          decoration: UnderlineDecoration(
                            color: ApplicationConstants.primaryColor,
                            textStyle: Theme.of(context).textTheme.display3,
                            hintText: "8888",
                            hintTextStyle:
                                Theme.of(context).textTheme.display3.copyWith(
                                      color: ThemeModel.isDarkTheme(context)
                                          ? ApplicationConstants
                                              .disabledDarkThemeColor
                                          : ApplicationConstants
                                              .disabledLightThemeColor,
                                    ),
                          ),
                          controller: pinEditingController,
                          autoFocus: false,
                          textInputAction: TextInputAction.done,
                          onSubmit: (pin) {
                            FocusScope.of(context).requestFocus(FocusNode());
                            debugPrint('submit pin:$pin');
                          },
                        ),
                      ),
                    ),
                    UiHelper.verticalSpace(36),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 43.0),
                      child: BusyButton(
                        onPressed: () {
                          model.verify(
                            code: int.tryParse(pinCodeValue, radix: 10),
                            phoneNumber: widget.phoneNumber,
                          );
                        },
                        busy: model.busy,
                        title: Strings.verify,
                      ),
                    ),
                    UiHelper.verticalSpace(8),
                    ViewModelProvider<ResendVerificationViewModel>.withConsumer(
                      builder: (context, model, _) {
                        return model.busy
                            ? Container(
                                height: 36,
                                width: 36,
                                padding: EdgeInsets.all(8),
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      ApplicationConstants.primaryColor),
                                ),
                              )
                            : Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 43.0),
                                child: FlatButton(
                                  onPressed: () {
                                    model.resendVerify(
                                        phoneNumber: widget.phoneNumber);
                                  },
                                  splashColor: Colors.transparent,
                                  child: RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: Strings.didntReceive,
                                          style: Theme.of(context)
                                              .textTheme
                                              .button,
                                        ),
                                        TextSpan(
                                          text: Strings.resend,
                                          style: Theme.of(context)
                                              .textTheme
                                              .button
                                              .copyWith(
                                                  color: ApplicationConstants
                                                      .primaryColor),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                      },
                      viewModel: ResendVerificationViewModel(),
                    ),
                  ],
                ),
              );
            },
          )
//            UiHelper.verticalSpace(16),
        ],
      ),
    );
  }
}
