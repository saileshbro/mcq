import 'package:flutter/material.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/verification_response_model.dart';
import 'package:mcq/common/services/authentication/repository/authentication_repository.dart';
import 'package:mcq/core/models/base_view_model.dart';
import 'package:mcq/core/models/failure.dart';
import 'package:mcq/core/services/dialog_service.dart';
import 'package:mcq/core/services/navigation_service.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/dashboard/dashboard.dart';

import '../../../locator.dart';

class VerificationViewModel extends BaseViewModel {
  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();

  UserDataService _userDataService = UserDataService();
  NavigationService _navigationService = locator<NavigationService>();
  DialogService _dialogService = locator<DialogService>();

  Future verify({@required String phoneNumber, @required int code}) async {
    setBusy(true);
    try {
      String _phone = phoneNumber.trim();
      VerificationResponseModel verificationResponseModel =
          await _authenticationRepository.verify(
        verificationRequestModel: VerificationRequestModel(
          code: code,
          phone: _phone,
        ),
      );
      setBusy(false);
      _userDataService.saveData(
          verificationResponseModel.token, verificationResponseModel.name);

      _dialogService
          .showDialog(
        title: "Verification successful",
        description: verificationResponseModel.message,
        buttonTitle: "Ok",
      )
          .then((_) {
        _navigationService.navigateTo(Dashboard(), clearStack: true);
      });
    } on Failure catch (e) {
      setBusy(false);
      _dialogService.showDialog(
        title: "Verification Failure",
        description: e.message,
        buttonTitle: "Ok",
      );
    }
  }

  Future resendVerify({@required String phoneNumber}) async {
    setBusy(true);
    try {
      String _phone = phoneNumber.trim();
      VerificationResponseModel verificationResponseModel =
          await _authenticationRepository.resendVerify(
        verificationRequestModel: VerificationRequestModel(
          phone: _phone,
        ),
      );
      setBusy(false);
      _dialogService.showDialog(
        title: "Verification code",
        description: verificationResponseModel.message,
        buttonTitle: "Ok",
      );
    } on Failure catch (e) {
      setBusy(false);
      _dialogService.showDialog(
        title: "Error",
        description: e.message,
        buttonTitle: "Ok",
      );
    }
  }
}
