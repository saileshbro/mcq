import 'package:mcq/common/services/authentication/authentication_data_source/models/register_request_model.dart';
import 'package:mcq/common/services/authentication/authentication_data_source/models/register_response_model.dart';
import 'package:mcq/common/services/authentication/repository/authentication_repository.dart';
import 'package:mcq/core/models/base_view_model.dart';
import 'package:mcq/core/models/failure.dart';
import 'package:mcq/core/services/dialog_service.dart';
import 'package:mcq/core/services/navigation_service.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/features/register/views/verification_view.dart';
import 'package:mcq/locator.dart';

class RegisterViewModel extends BaseViewModel {
  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();

  UserDataService _userDataService = UserDataService();
  NavigationService _navigationService = locator<NavigationService>();
  DialogService _dialogService = locator<DialogService>();

  Future register(String name, String phoneNumber, String password) async {
    setBusy(true);

    try {
      String _name = name.trim();
      String _phoneNumber = phoneNumber.trim();
      String _password = password.trim();

      RegisterResponseModel registerResponseModel =
          await _authenticationRepository.register(
        registerRequestModel: RegisterRequestModel(
          name: _name,
          phone: _phoneNumber,
          password: _password,
        ),
      );

      // TODO: add token in User Data Service

      _dialogService
          .showDialog(
        title: "Register Successful",
        description: registerResponseModel.message,
        buttonTitle: "Okay",
      )
          .then((_) {
        _navigationService.navigateTo(
          VerificationView(
            phoneNumber: _phoneNumber,
          ),
        );
      });
    } on Failure catch (e) {
      _dialogService.showDialog(
        title: "Register Failure",
        description: e.message,
        buttonTitle: "Ok",
      );
    }

    setBusy(false);
  }
}
