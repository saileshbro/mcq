import 'package:flutter/material.dart';
import 'package:mcq/common/ui/custom_popup_menu_button.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/section_app_bar.dart';

class ResultsView extends StatefulWidget {
  final HomeTileModel homeTileModel;

  const ResultsView({Key key, @required this.homeTileModel}) : super(key: key);

  @override
  _ResultsViewState createState() => _ResultsViewState();
}

class _ResultsViewState extends State<ResultsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SectionAppBar(
            homeTileModel: widget.homeTileModel,
            curvedButtonChild: CustomPopupMenuButton(),
          )
        ],
      ),
    );
  }
}
