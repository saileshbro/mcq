import 'package:flutter/material.dart';
import 'package:mcq/common/ui/custom_popup_menu_button.dart';
import 'package:mcq/common/ui/home_tile_model.dart';
import 'package:mcq/common/ui/section_app_bar.dart';

class ExamsView extends StatefulWidget {
  final HomeTileModel homeTileModel;

  const ExamsView({Key key, @required this.homeTileModel}) : super(key: key);

  @override
  _ExamsViewState createState() => _ExamsViewState();
}

class _ExamsViewState extends State<ExamsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SectionAppBar(
            homeTileModel: widget.homeTileModel,
            curvedButtonChild: CustomPopupMenuButton(),
          )
        ],
      ),
    );
  }
}
