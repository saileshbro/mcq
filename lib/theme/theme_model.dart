import 'package:flutter/material.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/core/models/base_view_model.dart';
import 'package:mcq/theme/theme.dart';

enum THEMETYPE { DARK, LIGHT }

class MCQTheme {
  static int getId(THEMETYPE themeType) {
    switch (themeType) {
      case THEMETYPE.LIGHT:
        return 0;
        break;
      case THEMETYPE.DARK:
        return 1;
        break;
      default:
        return 1;
        break;
    }
  }

  static THEMETYPE getTheme(int id) {
    switch (id) {
      case 0:
        return THEMETYPE.LIGHT;
        break;
      case 1:
        return THEMETYPE.DARK;
        break;
      default:
        return THEMETYPE.DARK;
        break;
    }
  }
}

class ThemeModel extends BaseViewModel {
  THEMETYPE themeType;

  Brightness get statusBarBrightness => themeType == THEMETYPE.DARK
      ? lightThemeData.brightness
      : darkThemeData.brightness;

  ThemeData get getTheme {
    int id = ApplicationConstants.preferences.getInt("THEME");
    if (id != null) {
      themeType = MCQTheme.getTheme(id);
    }

    switch (themeType) {
      case THEMETYPE.LIGHT:
        return lightThemeData;
        break;
      case THEMETYPE.DARK:
        return darkThemeData;
        break;
      default:
        return darkThemeData;
        break;
    }
  }

  switchTheme() {
    if (themeType == THEMETYPE.DARK) {
      changeTheme(THEMETYPE.LIGHT);
    } else {
      changeTheme(THEMETYPE.DARK);
    }
  }

  changeTheme(THEMETYPE themetype) {
    this.themeType = themetype;
    ApplicationConstants.preferences.setInt("THEME", MCQTheme.getId(themetype));
    notifyListeners();
  }

  static bool isDarkTheme(BuildContext context) =>
      Theme.of(context).brightness == Brightness.dark;
//
//  ThemeModel._();
//
//  factory ThemeModel() {
//    return _instance;
//  }
//
//  static final _instance = ThemeModel._();
}
