import 'package:flutter/material.dart';
import 'package:mcq/constants/application_constants.dart';

final ThemeData darkThemeData = ThemeData(
  brightness: Brightness.dark,
  accentColor: ApplicationConstants.primaryColor,
  cursorColor: ApplicationConstants.primaryColor,
  cardColor: ApplicationConstants.primaryMediumTextColor,
  appBarTheme: AppBarTheme(
    color: ApplicationConstants.primaryDarkTextColor,
    elevation: 0.0,
    brightness: Brightness.light,
    iconTheme: IconThemeData(
      color: ApplicationConstants.textFieldBackgroundColor,
    ),
  ),
  scaffoldBackgroundColor: ApplicationConstants.primaryDarkTextColor,
  primaryIconTheme:
      IconThemeData(color: ApplicationConstants.textFieldBackgroundColor),
  inputDecorationTheme: InputDecorationTheme(
    fillColor: ApplicationConstants.primaryMediumTextColor,
    filled: true,
    border: OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  popupMenuTheme: PopupMenuThemeData(
    textStyle: ThemeData.dark().textTheme.subtitle,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  bottomAppBarColor: Colors.black,
  buttonTheme: ButtonThemeData(
    buttonColor: ApplicationConstants.primaryColor,
    textTheme: ButtonTextTheme.primary,
    height: 48,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  textTheme: TextTheme(
    display1: ThemeData.dark().textTheme.display1.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 96,
          fontWeight: FontWeight.w300,
        ),
    display2: ThemeData.dark().textTheme.display2.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 60,
          fontWeight: FontWeight.w600,
        ),
    display3: ThemeData.dark().textTheme.display3.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 20,
          fontWeight: FontWeight.w500,
          color: ApplicationConstants.textFieldBackgroundColor,
        ),
    display4: ThemeData.dark().textTheme.display4.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 34,
          fontWeight: FontWeight.w600,
          color: ApplicationConstants.textFieldBackgroundColor,
        ),
    body1: ThemeData.dark().textTheme.body1.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 18,
          fontWeight: FontWeight.normal,
          color: ApplicationConstants.primaryDarkTextColor,
        ),
    body2: ThemeData.dark().textTheme.body2.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 14,
          fontWeight: FontWeight.normal,
          color: ApplicationConstants.primaryDarkTextColor,
        ),
    button: ThemeData.dark().textTheme.button.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: ApplicationConstants.textFieldBackgroundColor,
        ),
    caption: ThemeData.dark().textTheme.caption.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 12,
          fontWeight: FontWeight.normal,
          color: ApplicationConstants.textFieldBackgroundColor,
        ),
    subtitle: ThemeData.dark().textTheme.subtitle.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: ApplicationConstants.textFieldBackgroundColor,
        ),
  ),
);
final ThemeData lightThemeData = ThemeData(
  brightness: Brightness.light,
  popupMenuTheme: PopupMenuThemeData(
    textStyle: ThemeData.light().textTheme.subtitle,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  cardColor: ApplicationConstants.textFieldBackgroundColor,
  accentColor: ApplicationConstants.primaryColor,
  cursorColor: ApplicationConstants.primaryColor,
  appBarTheme: AppBarTheme(
    color: ApplicationConstants.scaffoldBackgroundColor,
    elevation: 0.0,
    brightness: Brightness.dark,
  ),
  scaffoldBackgroundColor: ApplicationConstants.scaffoldBackgroundColor,
  primaryColor: ApplicationConstants.primaryColor,
  fontFamily: ApplicationConstants.fontFamily,
  primaryIconTheme: IconThemeData(
    color: ApplicationConstants.disabledLightThemeColor,
  ),
  inputDecorationTheme: InputDecorationTheme(
    fillColor: ApplicationConstants.textFieldBackgroundColor,
    filled: true,
    border: OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  buttonTheme: ButtonThemeData(
    buttonColor: ApplicationConstants.primaryColor,
    textTheme: ButtonTextTheme.primary,
    height: 48,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  buttonColor: ApplicationConstants.primaryColor,
  textTheme: TextTheme(
    display1: ThemeData.light().textTheme.display1.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 96,
          fontWeight: FontWeight.w300,
        ),
    display2: ThemeData.light().textTheme.display2.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 60,
          fontWeight: FontWeight.w600,
        ),
    display3: ThemeData.light().textTheme.display3.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 20,
          color: ApplicationConstants.primaryMediumTextColor,
          fontWeight: FontWeight.w500,
        ),
    subtitle: ThemeData.light().textTheme.subtitle.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
    display4: ThemeData.light().textTheme.display4.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 34,
          fontWeight: FontWeight.w600,
          color: ApplicationConstants.primaryDarkTextColor,
        ),
    body1: ThemeData.light().textTheme.body1.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 18,
          fontWeight: FontWeight.normal,
          color: ApplicationConstants.primaryDarkTextColor,
        ),
    body2: ThemeData.light().textTheme.body2.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 14,
          fontWeight: FontWeight.normal,
          color: ApplicationConstants.primaryDarkTextColor,
        ),
    button: ThemeData.light().textTheme.button.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: ApplicationConstants.primaryMediumTextColor,
        ),
    caption: ThemeData.light().textTheme.caption.copyWith(
          fontFamily: ApplicationConstants.fontFamily,
          fontSize: 12,
          fontWeight: FontWeight.normal,
          color: ApplicationConstants.primaryLightTextColor,
        ),
  ),
);
