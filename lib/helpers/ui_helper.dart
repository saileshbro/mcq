import 'package:flutter/material.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:shimmer/shimmer.dart';

class UiHelper {
  static SizedBox horizontalSpace(double space) {
    return SizedBox(width: space);
  }

  static SizedBox verticalSpace(double space) {
    return SizedBox(height: space);
  }

  static SizedBox horizontalAndverticalSpace(
      double horizontal, double vertical) {
    return SizedBox(height: vertical, width: horizontal);
  }

  static void showBottomSheet(
    BuildContext context, {
    Widget child,
    Function callOnThen,
    double bottomPadding = 32,
    Color barColor = ApplicationConstants.disabledLightThemeColor,
  }) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        isDismissible: false,
        builder: (BuildContext bc) {
          return Container(
            decoration: BoxDecoration(
              color: Theme.of(context).scaffoldBackgroundColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16), topRight: Radius.circular(16)),
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 16, bottom: bottomPadding),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    width: 72,
                    height: 4,
                    decoration: BoxDecoration(
                        color: barColor,
                        borderRadius: BorderRadius.circular(5)),
                  ),
                  SizedBox(height: 24),
                  child,
                ],
              ),
            ),
          );
        }).then((_) {
      if (callOnThen != null) callOnThen();
    });
  }

  static List<BoxShadow> getBoxShadow(BuildContext context, Color color) {
    return [
      if (Theme.of(context).brightness == Brightness.light)
        BoxShadow(
          offset: Offset(0, 4),
          blurRadius: 20,
          color: color.withOpacity(0.37),
        )
      else
        BoxShadow(
          blurRadius: 5,
          color: color.withOpacity(0.6),
        ),
    ];
  }

  static Widget shimmerContainer(
    double width,
    double height, {
    double radius = 0,
  }) =>
      ClipRRect(
        borderRadius: BorderRadius.circular(radius),
        child: Container(
          width: width,
          height: height,
          constraints: BoxConstraints.tight(Size(width, height)),
          child: Shimmer.fromColors(
            period: Duration(milliseconds: 1200),
            baseColor: Colors.grey.withOpacity(0.15),
            highlightColor: Colors.black12,
            child: Container(
              color: Colors.grey,
            ),
          ),
        ),
      );
}
