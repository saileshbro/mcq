import 'package:flutter/material.dart';

class BaseViewModel extends ChangeNotifier {
  /// state if model is loading or not
  bool _busy = false;
  bool get busy => _busy;

  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }
}
