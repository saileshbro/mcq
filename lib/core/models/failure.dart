import 'package:flutter/foundation.dart';

class Failure {
  int _code;
  String _message;

  int get code => _code;
  String get message => _message;

  Failure({int code, @required String message})
      : _code = code,
        _message = message;

  @override
  String toString() {
    return "MCQ Failure: Code: $code, Message $message";
  }
}
