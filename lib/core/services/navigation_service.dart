import 'package:flutter/material.dart';

import '../../common/ui/fade_in_page_transition.dart';

class NavigationService {
  final GlobalKey<NavigatorState> _navigationKey = GlobalKey<NavigatorState>();

  GlobalKey<NavigatorState> get navigationKey => _navigationKey;

  bool pop() {
    return _navigationKey.currentState.pop();
  }

  Future<dynamic> navigateToRoute(String routeName,
      {dynamic arguments, bool clearStack = false}) {
    return _navigationKey.currentState.pushNamedAndRemoveUntil(
      routeName,
      (_) => !clearStack,
      arguments: arguments,
    );
  }

  Future<dynamic> navigateTo(Widget widget, {bool clearStack = false}) {
    if (clearStack) {
      return _navigationKey.currentState
          .pushAndRemoveUntil(FadePageTransition(child: widget), (_) => false);
    }

    return _navigationKey.currentState.push(FadePageTransition(child: widget));
  }

  NavigationService._();

  factory NavigationService() {
    return _instance;
  }

  static final _instance = NavigationService._();
}
