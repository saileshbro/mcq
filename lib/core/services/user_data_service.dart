import 'package:mcq/constants/application_constants.dart';

final String TOKEN_KEY = "TOKEN";
final String NAME_KEY = "NAME";

class UserDataService {
  static const String TAG = "UserDataService";

  String _token;
  String _name;

  String get token => _token;

  String get name => _name;

  UserDataService._();

  factory UserDataService() {
    return _instance;
  }

  bool saveData(String token, String name) {
    _token = token;
    _name = name;
    ApplicationConstants.preferences.setString(TOKEN_KEY, token);
    ApplicationConstants.preferences.setString(NAME_KEY, name);

    print(TAG + " Data Saved");
    return true;
  }

  bool getData() {
    _token = ApplicationConstants.preferences.getString(TOKEN_KEY);
    _name = ApplicationConstants.preferences.getString(NAME_KEY);
    print(TAG + " Data Retrieved");
    return true;
  }

  bool clearData() {
    _token = null;
    _name = null;
    ApplicationConstants.preferences.remove(TOKEN_KEY);
    ApplicationConstants.preferences.remove(NAME_KEY);
    print(TAG + " Data Cleared");
    return true;
  }

  bool get isLoggedIn {
    getData();
    if (_token != null &&
        _token.length > 0 &&
        _name != null &&
        _name.length > 0) {
      print(TAG + " Logged In");
      return true;
    }
    print(TAG + " Not Logged In");
    return false;
  }

  static final _instance = UserDataService._();
}
