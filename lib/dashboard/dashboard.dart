import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mcq/application_wrapper.dart';
import 'package:mcq/features/courses/views/courses_view.dart';
import 'package:mcq/features/discussion/views/discussion_view.dart';
import 'package:mcq/features/home/views/home_view.dart';
import 'package:mcq/features/profile/views/profile_view.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ApplicationWrapper(
      pages: <Widget>[
        HomeView(),
        CourseView(),
        DiscussionView(),
        ProfileView(),
      ],
      navigationMenus: [
        BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
          ),
          title: Text(
            "Home",
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.book,
          ),
          title: Text(
            "Courses",
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.forum,
          ),
          title: Text(
            "Discussion",
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.person,
          ),
          title: Text(
            "Profile",
          ),
        ),
      ],
    );
  }
}
