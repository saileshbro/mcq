class Strings {
  static const String login = "Login";
  static const String forgotPassword = "Forgot Password?";
  static const String password = "Password";
  static const String dontHaveAccount = "Don’t have an account?";
  static const String phoneNumber = "Phone Number";
  static const String afterLogin =
      "After login, start your study career and wish you progress";
  static const String welcome = "Welcome";
  static const String register = "Register";
  static const String fullName = "Full Name";
  static const String alreadyHaveAnAccount = "Already have an account?";
  static const String learn = "Learn";
  static const String readOurBlog =
      "Read our blogs, take our courses, succeed in quizes,\n do project work and excel.";
  static const String practice = "Practice";
  static const String score = "Score";
  static const String home = "Home";
  static const String chooseYour = "Choose your course, ";
  static const String rightAway = "right away";
  static const String search = "Search for course, subject ....";
  static const String notes = "Notes";
  static const String videos = "Videos";
  static const String exams = "Exams";
  static const String calender = "Calender";
  static const String results = "Results";
  static const String recommendedCourses = "Recommended Courses";
  static const String lastViewed = "Last viewed, ";
  static const String physics = "Physics";
  static const String events = "events";
  static const String neverMissOut = "Never miss any useful ";
  static const String settings = "Setting";
  static const String nightMode = "Night mode";
  static const String requestNewPassword = "Request new password";
  static const String courses = "Courses";
  static const String discussions = "Discussions";
  static const String profile = "Profile";
  static const String verification = "Verification";
  static const String enterTheVerification =
      "Enter the verification code just sent to your phone.";
  static const String verify = "Verify";
  static const String didntReceive = "Didn’t receive a code? ";
  static const String resend = "Resend";
  static const String enroll = "Enroll";
  static const String mathematics = "Mathematics";
  static const String newPassword = "New Password";
  static const String changePassword = "Change Password";
  static const String otpCode = "OTP Code";
  static const String forgotPasswordMsg =
      "OTP Code has been sent to your phone, use it and change your password";
  static const String importantFormula = "Important Formulas";
  static const String courseNotes = "Course Notes";
  static const String status = "Status: ";
  static const String activelyInvolved = "Actively Involved";
  static const String highestRanking = "Highest Ranking ";
  static const String examHistory = "Exam history";
  static const String attemptedQuestion = "Attempted questions";
  static const String enrolledCourses = "Enrolled courses";
  static const String savedResources = "Saved resources";
  static const String availableCourses = "Available Courses";
  static const String free = "Free";
  static const String performanceHistory = "Performance history";
  static const String highestScores = "Highest scores";
  static const String practiceSets = 'Practice Sets';
  static const String next = "Next";
  static const String confused = 'Confused?';
  static const String skipForNow = "Skip for now";
}
