import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApplicationConstants {
  /// Global App related
  static const String appName = "MCQ";

  /// Theme related
  static const String fontFamily = "WorkSans";
  static const Color iconColor = Color(0xFF4A6572);
  static const Color primaryColor = Color(0xFF009688);
  static const Color primaryDarkTextColor = Color(0xFF1b1b1b);
  static const Color primaryLightTextColor = Color(0xFF4A6572);
  static const Color primaryMediumTextColor = Color(0xFF252525);
  static const Color scaffoldBackgroundColor = Color(0xFFFFFFFF);
  static const Color textFieldBackgroundColor = Color(0xFFF4F6FA);
  static const Color errorColor = Color(0xffb00020);
  static const Color textRed = Color(0xFFF96566);
  static const Color gapColor = Color(0xFFF2F2F2);
  static const Color colorPrice = Color(0xFFF86C35);
  static const Color disabledLightThemeColor = Color(0xFFbbbbbb);
  static const Color disabledDarkThemeColor = Color(0xFF444444);

  ///Assets related
  static const String loginIllustration =
      "assets/illustrations/login_illustration.png";
  static const String registerIllustration =
      "assets/illustrations/register_illustration.png";
  static const String verificationIllustration =
      "assets/illustrations/verification_illustration.png";
  static const String onboardingAsset1 =
      "assets/illustrations/onboardingAsset1.png";
  static const String onboardingAsset3 =
      "assets/illustrations/onboardingAsset2.png";
  static const String onboardingAsset2 =
      "assets/illustrations/onboardingAsset3.png";

  /// Home view tiles related colors
  static const Color notesColor = Color(0xFF3CC795);
  static const Color videosColor = Color(0xFFF96566);
  static const Color practiceColor = Color(0xFF4D9CFC);
  static const Color examsColor = Color(0xFF4D9CFC);
  static const Color calenderColor = Color(0xFFB364EB);
  static const Color resultsColor = Color(0xFF3CC795);
  static const Color notesGradientColor = Color(0xFF74E27B);
  static const Color videosGradientColor = Color(0xFFEE8182);
  static const Color practiceGradientColor = Color(0xFF5CC4FB);
  static const Color examsGradientColor = Color(0xFF5CC4FB);
  static const Color calenderGradientColor = Color(0xFFCA81FF);
  static const Color resultsGradientColor = Color(0xFF74E27B);
  static const Color questionsDarkColor = Color(0xFF888888);
  static const Color optionDarkBorderColor = Color(0xFF666666);

//  Preferences
  static SharedPreferences preferences;
}
