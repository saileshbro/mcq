import 'package:flutter/material.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/helpers/ui_helper.dart';
import 'package:mcq/theme/theme_model.dart';

class ApplicationWrapper extends StatefulWidget {
  final List<Widget> pages;
  final List<BottomNavigationBarItem> navigationMenus;

  const ApplicationWrapper(
      {Key key, @required this.pages, @required this.navigationMenus})
      : assert(pages != null),
        assert(navigationMenus != null),
        assert(navigationMenus.length == pages.length),
        super(key: key);

  @override
  _ApplicationWrapperState createState() => _ApplicationWrapperState();
}

class _ApplicationWrapperState extends State<ApplicationWrapper> {
  PageController controller;
  int currentIndex = 0;

  @override
  void initState() {
    controller = PageController(
      initialPage: currentIndex,
    );
    super.initState();
  }

  changeIndex(int i) {
    setState(() {
      currentIndex = i;
      controller.animateToPage(
        i,
        duration: Duration(milliseconds: 200),
        curve: Curves.easeInOut,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
            boxShadow: UiHelper.getBoxShadow(
                context, ApplicationConstants.primaryColor)),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          elevation: 0.0,
          backgroundColor: Theme.of(context).bottomAppBarColor,
          currentIndex: currentIndex,
          onTap: changeIndex,
          selectedItemColor: ApplicationConstants.primaryColor,
          unselectedItemColor: ThemeModel.isDarkTheme(context)
              ? ApplicationConstants.disabledDarkThemeColor
              : ApplicationConstants.disabledLightThemeColor,
          unselectedLabelStyle: TextStyle(
              color: ThemeModel.isDarkTheme(context)
                  ? ApplicationConstants.disabledDarkThemeColor
                  : ApplicationConstants.disabledLightThemeColor),
          items: this.widget.navigationMenus,
        ),
      ),
      body: PageView(
        controller: controller,
        physics: NeverScrollableScrollPhysics(),
        children: widget.pages,
      ),
    );
  }
}
