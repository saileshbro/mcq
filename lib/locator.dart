import 'package:get_it/get_it.dart';
import 'package:mcq/common/services/authentication/repository/authentication_repository_impl.dart';
import 'package:mcq/common/services/home/home_data_source/local/home_local_ds.dart';
import 'package:mcq/common/services/home/home_data_source/local/home_local_ds_impl.dart';
import 'package:mcq/common/services/home/home_data_source/local/home_local_mock_ds.dart';
import 'package:mcq/common/services/home/home_data_source/remote/home_remote_ds.dart';
import 'package:mcq/common/services/home/home_data_source/remote/home_remote_mock_ds.dart';
import 'package:mcq/common/services/home/repository/home_repository.dart';
import 'package:mcq/common/services/home/repository/home_repository_impl.dart';
import 'package:mcq/core/config/global_config.dart';

import 'common/services/authentication/authentication_data_source/local/authentication_local_ds.dart';
import 'common/services/authentication/authentication_data_source/local/authentication_local_ds_impl.dart';
import 'common/services/authentication/authentication_data_source/local/authentication_local_mock_ds.dart';
import 'common/services/authentication/authentication_data_source/remote/authentication_remote_ds.dart';
import 'common/services/authentication/authentication_data_source/remote/authentication_remote_ds_impl.dart';
import 'common/services/authentication/authentication_data_source/remote/authentication_remote_mock_ds.dart';
import 'common/services/authentication/repository/authentication_repository.dart';
import 'common/services/home/home_data_source/remote/home_remote_ds_impl.dart';
import 'core/services/dialog_service.dart';
import 'core/services/http_service.dart';
import 'core/services/navigation_service.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
// core dependencies

  // http service
  locator.registerLazySingleton<HttpService>(() => HttpService());

  // navigation dependency
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
  // add dialog dependency
  locator.registerLazySingleton<DialogService>(() => DialogService());

  // features dependencies
  _initAuthenticationDependency();
}

/// to add new feature dependency create a _setup<feature-name> function and add call in setupLocator

_initAuthenticationDependency() {
// local data source
// if mock configuration => provide local mock data source
// else => provide local data source implementation
  locator.registerLazySingleton<AuthenticationLDS>(
    () => (currentBuildConfig == AppBuildConfig.MOCK)
        ? AuthenticationLMDS()
        : AuthenticationLDSImpl(),
  );

// remote data source
// if mock configuration => provide local mock data source
// else => provide local data source implementation
  locator.registerLazySingleton<AuthenticationRDS>(
    () => (currentBuildConfig == AppBuildConfig.MOCK)
        ? AuthenticationRMDS()
        : AuthenticationRDSImpl(
            locator<HttpService>(),
          ),
  );

  locator.registerLazySingleton<HomeLDS>(() =>
      (currentBuildConfig == AppBuildConfig.MOCK) ? HomeLMDS() : HomeLDSImpl());
  locator.registerLazySingleton<HomeRDS>(
    () => (currentBuildConfig == AppBuildConfig.MOCK)
        ? HomeRMDS()
        : HomeRDSImpl(
            locator<HttpService>(),
          ),
  );
// repository
  locator.registerLazySingleton<AuthenticationRepository>(
    () => AuthenticationRepositoryImpl(
      localDataSource: locator<AuthenticationLDS>(),
      remoteDataSource: locator<AuthenticationRDS>(),
    ),
  );
  locator.registerLazySingleton<HomeRepository>(
    () => HomeRepositoryImpl(
      localDataSource: locator<HomeLDS>(),
      remoteDataSource: locator<HomeRDS>(),
    ),
  );
}
