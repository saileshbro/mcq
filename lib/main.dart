import 'package:flutter/material.dart';
import 'package:mcq/constants/application_constants.dart';
import 'package:mcq/core/services/user_data_service.dart';
import 'package:mcq/dashboard/dashboard.dart';
import 'package:mcq/locator.dart';
import 'package:mcq/theme/theme_model.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/managers/dialog_managers.dart';
import 'core/services/dialog_service.dart';
import 'core/services/navigation_service.dart';
import 'features/login/views/login_view.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
//  SystemChrome.setSystemUIOverlayStyle(
//    SystemUiOverlayStyle(
//      statusBarBrightness: Brightness.light,
//      statusBarIconBrightness: Brightness.light,
//    ),
//  );
  ApplicationConstants.preferences = await SharedPreferences.getInstance();
  setupLocator();
  UserDataService().getData();

  runApp(ViewModelProvider<ThemeModel>.withConsumer(
    builder: (context, model, _) => Theme(
      data: model.getTheme,
      child: MyApp(themeModel: model),
    ),
    viewModel: ThemeModel(),
  ));
}

class MyApp extends StatefulWidget {
  final ThemeModel themeModel;

  const MyApp({
    Key key,
    this.themeModel,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: ApplicationConstants.appName,
      builder: (context, child) => Navigator(
        key: locator<DialogService>().dialogNavigationKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => DialogManager(
            child: child,
          ),
        ),
      ),
      navigatorKey: locator<NavigationService>().navigationKey,
      theme: widget.themeModel.getTheme,
      home: ThemeProvider(),
    );
  }
}

class ThemeProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("Check");
    return ViewModelProvider<ThemeModel>.withConsumer(
      builder: (context, model, _) => Theme(
        data: model.getTheme,
        child: UserDataService().isLoggedIn ? Dashboard() : LoginView(),
//        child: Dashboard(),
      ),
      viewModel: ThemeModel(),
    );
  }
}
